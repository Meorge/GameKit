﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit
{
    public static class Extensions
    {
        // https://stackoverflow.com/a/14919319
        public static void Fill<T>(this T[] originalArray, T with)
        {
            for (int i = 0; i < originalArray.Length; i++)
            {
                originalArray[i] = with;
            }
        }

        public static Vector2 ToVector2(this Vector3 v)
        {
            return new Vector2(v.X, v.Y);
        }

        public static Vector3 ToVector3(this Vector2 v)
        {
            return new Vector3(v.X, v.Y, 0);
        }

        public static Color Multiply(this Color a, Color b)
        {
            float R1, G1, B1, A1;
            float R2, G2, B2, A2;
            a.Deconstruct(out R1, out G1, out B1, out A1);
            b.Deconstruct(out R2, out G2, out B2, out A2);

            Color product = new Color(
                (R1 * R2),
                (G1 * G2),
                (B1 * B2),
                (A1 * A2)
                );

            return product;
        }

        public static Color MultiplyBrightness(this Color c, float amount)
        {
            c.R = (byte)(c.R * amount);
            c.G *= (byte)(c.G * amount);
            c.B *= (byte)(c.B * amount);
            return c;
        }

        //public static Color FromHexCode(string code)
        //{
        //    string redComp = code.Substring(0, 2);
        //    string greenComp = code.Substring(2, 2);
        //    string blueComp = code.Substring(4, 2);

        //    Console.WriteLine($"Make a color with components {redComp} {greenComp} {blueComp}");
        //    return Color.Wheat;
        //}
    }
}
