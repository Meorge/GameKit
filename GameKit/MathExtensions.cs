﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit
{
    public static class GKMath
    {
        public static readonly float DegToRad = 0.0174533f;
        public static float Lerp(float a, float b, float t)
        {
            return (1.0f - t) * a + b * t;
        }

        public static float InverseLerp(float a, float b, float v)
        {
            return (v - a) / (b - a);
        }

        public static float Remap(float iMin, float iMax, float oMin, float oMax, float v)
        {
            float t = InverseLerp(iMin, iMax, v);
            return Lerp(oMin, oMax, t);
        }


        public static Vector2 RandomPointOnCircleEdge(float radius)
        {
            float randomRadians = (float)Engine.rand.NextDouble() * (2 * MathF.PI);

            Vector2 point = new Vector2(MathF.Cos(randomRadians), MathF.Sin(randomRadians));

            point *= radius;
            return point;

        }

        public static float EulerZ(this Quaternion q)
        {
            return MathF.Atan2(2 * (q.W * q.Z + q.Y * q.Y), 1 - 2 * (q.Y * q.Y + q.Z * q.Z));
        }

        public static int RoundToInt(this float f)
        {
            return (int)MathF.Round(f);
        }

        public static Rectangle[] CreateNineSlice(Rectangle rect, int leftPadding, int rightPadding, int topPadding, int bottomPadding, float multiplier = 1)
        {
            int x = rect.X;
            int y = rect.Y;
            int width = rect.Width;
            int height = rect.Height;

            leftPadding = ((float)leftPadding * multiplier).RoundToInt();
            rightPadding = ((float)rightPadding * multiplier).RoundToInt();
            topPadding = ((float)topPadding * multiplier).RoundToInt();
            bottomPadding = ((float)bottomPadding * multiplier).RoundToInt();

            int middleWidth = width - leftPadding - rightPadding;
            int middleHeight = height - topPadding - bottomPadding;
            int bottomY = y + height - bottomPadding;
            int rightX = x + width - rightPadding;
            int leftX = x + leftPadding;
            int topY = y + topPadding;

            // TOP LEFT
            Rectangle destRectTL = new Rectangle(
                x,
                y,
                leftPadding,
                topPadding
                );

            // TOP CENTER
            Rectangle destRectTC = new Rectangle(
                leftX,
                y,
                middleWidth,
                topPadding
                );

            // TOP RIGHT
            Rectangle destRectTR = new Rectangle(
                rightX,
                y,
                rightPadding,
                topPadding
                );

            // CENTER LEFT
            Rectangle destRectCL = new Rectangle(
                x,
                topY,
                leftPadding,
                middleHeight
                );

            // CENTER CENTER
            Rectangle destRectCC = new Rectangle(
                leftX,
                topY,
                middleWidth,
                middleHeight
                );

            // CENTER RIGHT
            Rectangle destRectCR = new Rectangle(
                rightX,
                topY,
                rightPadding,
                middleHeight
                );

            // BOTTOM LEFT
            Rectangle destRectBL = new Rectangle(
                x,
                bottomY,
                leftPadding,
                bottomPadding
                );

            // BOTTOM CENTER
            Rectangle destRectBC = new Rectangle(
                leftX,
                bottomY,
                middleWidth,
                bottomPadding
                );

            // BOTTOM RIGHT
            Rectangle destRectBR = new Rectangle(
                rightX,
                bottomY,
                rightPadding,
                bottomPadding
                );


            return new Rectangle[]
            {
                destRectTL,
                destRectTC,
                destRectTR,

                destRectCL,
                destRectCC,
                destRectCR,

                destRectBL,
                destRectBC,
                destRectBR
            };
        }


    }
}
