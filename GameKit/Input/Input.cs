﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameKit.InputManagement
{
    public static class Input
    {
        static KeyboardState previousKeyboard;
        static KeyboardState currentKeyboard;

        static MouseState previousMouse;
        static MouseState currentMouse;

        public static float ClickDuration = 0.2f;

        private static Timer leftClickTimer, middleClickTimer, rightClickTimer;

        private static bool hasBeenInitialized = false;

        static void Initialize()
        {
            leftClickTimer = Timer.Empty();
            middleClickTimer = Timer.Empty();
            rightClickTimer = Timer.Empty();

            hasBeenInitialized = true;
        }

        public static void Update()
        {
            if (!hasBeenInitialized) Initialize();

            previousKeyboard = currentKeyboard;
            currentKeyboard = Keyboard.GetState();

            previousMouse = currentMouse;
            currentMouse = Mouse.GetState();

            if (MouseButtonPressedIn(MouseButton.LeftButton))
                leftClickTimer = new Timer(ClickDuration);
            if (MouseButtonPressedIn(MouseButton.MiddleButton))
                middleClickTimer = new Timer(ClickDuration);
            if (MouseButtonPressedIn(MouseButton.RightButton))
                rightClickTimer = new Timer(ClickDuration);
        }

        public static bool KeyReleased(Keys key)
        {
            return (previousKeyboard.IsKeyDown(key) && currentKeyboard.IsKeyUp(key));
        }

        public static bool KeyPressedIn(Keys key)
        {
            return (previousKeyboard.IsKeyUp(key) && currentKeyboard.IsKeyDown(key));
        }

        public static bool KeyPressed(Keys key)
        {
            return currentKeyboard.IsKeyDown(key);
        }

        public static Vector2 CursorPosition(PositionSpace space = PositionSpace.CameraSpace)
        {
            MouseState s = Mouse.GetState();
            Vector2 v = new Vector2(s.X, s.Y);

            if (space == PositionSpace.CameraSpace) return v;
            else
            {
                return Engine.engine.GetActiveCamera().ToWorldSpace(v);
            }
        }

        public enum MouseButton
        {
            LeftButton,
            RightButton,
            MiddleButton
        }

        public static bool MouseButtonPressed(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.LeftButton:
                    return currentMouse.LeftButton == ButtonState.Pressed;
                case MouseButton.RightButton:
                    return currentMouse.RightButton == ButtonState.Pressed;
                case MouseButton.MiddleButton:
                    return currentMouse.MiddleButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public static bool MouseButtonPressedIn(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.LeftButton:
                    return currentMouse.LeftButton == ButtonState.Pressed && previousMouse.LeftButton == ButtonState.Released;
                case MouseButton.RightButton:
                    return currentMouse.RightButton == ButtonState.Pressed && previousMouse.RightButton == ButtonState.Released;
                case MouseButton.MiddleButton:
                    return currentMouse.MiddleButton == ButtonState.Pressed && previousMouse.MiddleButton == ButtonState.Released;
                default:
                    return false;
            }
        }

        public static bool MouseButtonReleased(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.LeftButton:
                    return currentMouse.LeftButton == ButtonState.Released && previousMouse.LeftButton == ButtonState.Pressed;
                case MouseButton.RightButton:
                    return currentMouse.RightButton == ButtonState.Released && previousMouse.RightButton == ButtonState.Pressed;
                case MouseButton.MiddleButton:
                    return currentMouse.MiddleButton == ButtonState.Released && previousMouse.MiddleButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public static bool MouseButtonClicked(MouseButton button)
        {
            bool released = MouseButtonReleased(button);
            if (!released) return false;

            switch (button)
            {
                case MouseButton.LeftButton:
                    return released && !leftClickTimer.Depleted;
                case MouseButton.MiddleButton:
                    return released && !middleClickTimer.Depleted;
                case MouseButton.RightButton:
                    return released && !rightClickTimer.Depleted;
                default:
                    return false;
            }
        }

        public static int MouseScrollWheelDelta(ScrollWheelAxis axis = ScrollWheelAxis.X)
        {
            switch (axis)
            {
                case ScrollWheelAxis.Y:
                    return currentMouse.ScrollWheelValue - previousMouse.ScrollWheelValue;
                case ScrollWheelAxis.X:
                    return currentMouse.HorizontalScrollWheelValue - previousMouse.HorizontalScrollWheelValue;
                default:
                    return 0;
            }
        }

        public static Vector2 MouseMovementDelta()
        {
            return new Vector2(currentMouse.X - previousMouse.X, currentMouse.Y - previousMouse.Y);
        }

        public enum ScrollWheelAxis
        {
            X = 0,
            Y = 1
        }

    }
}
