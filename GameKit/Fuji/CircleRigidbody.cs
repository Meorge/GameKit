﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Fuji
{
    public class CircleRigidbody : Rigidbody
    {
        public override float DragCoefficient
        {
            get
            {
                return 0.5f;
            }
        }

        public override float ReferenceArea
        {
            get
            {
                return MathF.PI * (float)(gameObject.Width / 2);
            }
        }

        public bool CollidingWithOtherCircle(CircleRigidbody r)
        {
            float myRadius = gameObject.Width / 2;
            float otherRadius = r.gameObject.Width / 2;

            float distanceBetween = Vector2.Distance(gameObject.AbsolutePosition, r.gameObject.AbsolutePosition);

            return (distanceBetween < myRadius + otherRadius);
        }


        public override bool InRangeOfForce(Force f)
        {
            return true;
            //float radius = gameObject.Width / 2;

            //float forceRadius = f.Radius;

            //float distanceFromRBToForce = Vector2.Distance(gameObject.AbsolutePosition, f.Origin);

            //return distanceFromRBToForce < (radius + forceRadius);
        }
    }
}
