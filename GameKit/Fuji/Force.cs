﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Fuji
{
    public class Force
    {
        Vector2 _force;

        public ConstrainedValue Duration;


        public Vector2 ForceVector
        {
            get
            {
                return _force;
            }

            set
            {
                _force = value;
            }
        }

        public Force(Vector2 force, float duration = 0f)
        {
            _force = force;

            Duration = new ConstrainedValue(duration);
        }
    }
}
