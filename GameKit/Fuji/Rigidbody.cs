﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace GameKit.Fuji
{
    public class Rigidbody : Component
    {
        protected float mass = 1f;
        protected Vector2 velocity = Vector2.Zero;

        virtual public float DragCoefficient
        {
            get { return 1f; }
        }

        virtual public float ReferenceArea
        {
            get { return 1f; }
        }

        protected Vector2 forces = Vector2.Zero;

        protected int numberOfForces = 0;

        protected Vector2 accumulatedMovement = Vector2.Zero;

        FujiEngine fuji;

        public float Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        // Momentum
        private Vector2 previousMomentum;
        public Vector2 Momentum
        {
            get { return mass * Velocity; }
        }
        public Vector2 Impulse
        {
            get { return Momentum - previousMomentum; }
        }


        // Velocity
        private Vector2 previousVelocity;
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        public Vector2 Acceleration
        {
            get { return Velocity - previousVelocity; }
        }

        public List<Rigidbody> OtherRigidbodies
        {
            get
            {
                List<Rigidbody> b = FujiEngine.Rigidbodies;
                b.Remove(this);
                b.RemoveAll((r) => !Engine.engine.gameObjects.Contains(r.gameObject));
                return b;
            }
        }

        public override void Create()
        {
            fuji = Engine.engine.GetSubEngine<FujiEngine>();
            fuji.AddRigidbody(this);
        }

        virtual public bool InRangeOfForce(Force f) { return false; }

        public void AddForce(Vector2 f)
        {
            AddForce(new Force(f));
        }

        public void AddForce(Force f)
        {
            if (float.IsNaN(f.ForceVector.X) || float.IsNaN(f.ForceVector.Y))
            {
                Console.WriteLine("force is nan, don't add it");
                return;
            }
            forces += f.ForceVector;
            numberOfForces++;
        }

        public void ClearForces() {
            numberOfForces = 0;
            forces = Vector2.Zero;
        }

        void UpdateDerivatives()
        {
            previousVelocity = Velocity;
            previousMomentum = Momentum;
        }
        public void UpdateVelocity(GameTime gT)
        {
            // update our derivativey things
            UpdateDerivatives();

            // F = ma
            // F/m = a
            Vector2 acceleration = forces / mass;

            float elapsed = (float)gT.ElapsedGameTime.TotalSeconds;
            acceleration *= elapsed;

            // Add acceleration to velocity
            velocity += acceleration;

            accumulatedMovement += velocity * elapsed;

            // get integer part of accumulated movement
            Vector2 intMovement = new Vector2(MathF.Truncate(accumulatedMovement.X), MathF.Truncate(accumulatedMovement.Y));

            accumulatedMovement -= intMovement;

            gameObject.RawX += (int)intMovement.X;
            gameObject.RawY += (int)intMovement.Y;

            ClearForces();
        }
    }
}
