﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace GameKit.Fuji
{
    public class FujiEngine : Component
    {
        public static readonly float GravitationalConstant = 6.67e-11f;
        static List<Rigidbody> rigidbodies = new List<Rigidbody>();

        //public static int Ticks = 0;

        public static List<Rigidbody> Rigidbodies
        {
            get
            {
                return new List<Rigidbody>(rigidbodies);
            }
        }

        public void AddRigidbody(Rigidbody r)
        {
            rigidbodies.Add(r);
        }

        public override void Update(GameTime gameTime)
        {
            // iterate through all them rigidbodies
            foreach (Rigidbody r in rigidbodies)
            {
                if (Engine.engine.gameObjects.Contains(r.gameObject)) r.UpdateVelocity(gameTime);
            }

            //Ticks++;
        }

        public static Force GravitationalForce(Rigidbody me, Rigidbody other)
        {
            // Define the displacement vector r
            Vector2 r = other.gameObject.AbsolutePosition - me.gameObject.AbsolutePosition;

            // Scalar part of gravitational force formula (-GMm / r^2)
            float scalar = -1 * (GravitationalConstant * me.Mass * other.Mass) / r.LengthSquared();

            // Multiply displacement vector by scalar
            Vector2 forceVector = scalar * Vector2.Normalize(r);

            return new Force(forceVector);
        }

        public static Vector2 VelocityAfterCollision(Rigidbody a, Rigidbody b)
        {
            float bigNum = ((2 * b.Mass) / (a.Mass + b.Mass));

            
            bigNum *= Vector2.Dot(a.Velocity - b.Velocity, a.gameObject.AbsolutePosition - b.gameObject.AbsolutePosition) / (a.gameObject.AbsolutePosition - b.gameObject.AbsolutePosition).LengthSquared();

            Vector2 d = bigNum * (a.gameObject.AbsolutePosition - b.gameObject.AbsolutePosition);
            Vector2 veloc = a.Velocity - d;
            return veloc;
        }


        public static Vector2 VelocityAfterCollisionCOM(Rigidbody a, Rigidbody b)
        {
            // Find velocity of center of mass
            Vector2 centerOfMassVelocity = (a.Momentum + b.Momentum) / (a.Mass + b.Mass);

            //// Find velocity of two rigidbodies with respect to their center of mass velocity
            //Vector2 finalVelocityA_COM = a.Velocity - centerOfMassVelocity;
            //Vector2 finalVelocityB_COM = b.Velocity - centerOfMassVelocity;

            //// Flip the signs
            //finalVelocityA_COM *= -1;
            //finalVelocityB_COM *= -1;

            //// return to global frame
            //finalVelocityA_COM += centerOfMassVelocity;
            //finalVelocityB_COM += centerOfMassVelocity;

            return 2 * centerOfMassVelocity - a.Velocity;
        }

        public static Force DragForce(float massDensity, Rigidbody rb)
        {
            float dragAmount = 0.5f * massDensity * rb.Velocity.LengthSquared() * rb.DragCoefficient * rb.ReferenceArea;
            Vector2 vec = rb.Velocity == Vector2.Zero ? Vector2.Zero : Vector2.Normalize(rb.Velocity) * -dragAmount;
            return new Force(vec);
        }

        public static Vector2 OrbitalVelocity(Rigidbody big, Rigidbody small)
        {
            float scalar = MathF.Sqrt((GravitationalConstant * big.Mass) / Vector2.Distance(big.gameObject.Position2D, small.gameObject.Position2D));

            Vector2 smallToBig = Vector2.Normalize(big.gameObject.Position2D - small.gameObject.Position2D);

            //return smallToBig;
            return (new Vector2(smallToBig.Y, -smallToBig.X)) * scalar;
        }
    }
}
