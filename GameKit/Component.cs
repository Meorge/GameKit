﻿using System;
using Microsoft.Xna.Framework;

using GameKit.Components.Drawing;

namespace GameKit
{
    public class Component : IDisposable
    {

        private bool active = true;

        private bool createRun = false;

        public bool Active
        {
            get
            {
                return active;
            }

            set
            {
                active = value;
            }
        }

        public GameObject gameObject;


        public Component()
        {
        }

        virtual public void Create() { }

        public void _InternalUpdate(GameTime gameTime)
        {
            if (active && !createRun)
            {
                createRun = true;
                Create();
            }

            if (active) Update(gameTime);
        }

        virtual public void Update(GameTime gameTime) { }


        public void _InternalDraw(GameTime gameTime, Camera camera)
        {
            if (active) Draw(gameTime, camera);
        }

        virtual public void Draw(GameTime gameTime, Camera camera) { }

        virtual public void Dispose()
        {
            
        }
    }

    public static class ComponentFluentExtensions
    {
        public static T SetActive<T>(this T obj, bool a) where T : Component
        {
            obj.Active = a;
            return obj;
        }
    }
}


