﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace GameKit.Utilities
{
    public class TagParser
    {
        public char openingChar = '<';
        public char endingChar = '/';
        public char closingChar = '>';
        public char commandChar = '$';

        public List<TagObject> tags = new List<TagObject>();

        private Stack<TagObject> tagStack = new Stack<TagObject>();

        private string _humanReadableText;

        public string HumanReadableText
        {
            get
            {
                return _humanReadableText;
            }
        }

        public static string GetHumanReadableText(string text)
        {
            TagParser p = new TagParser();
            p.ParseText(text);
            return p.HumanReadableText;
        }

        public void ParseText(string text)
        {
            tags.Clear();
            tagStack.Clear();
            _humanReadableText = _InternalParseText(text);

            //Console.WriteLine("===== ALL DONE PARSING =====");

            //foreach (TagObject t in tags)
            //{
            //    Console.WriteLine($"TAG {{ name:\"{t.Name}\" arguments:\"{t.RawArguments}\" range:{t.startIndex}-{t.endIndex} }}");
            //}
        }

        string _InternalParseText(string text)
        {
            //Console.WriteLine("===== PARSE THAT TEXT MY DUDE =====");
            //Console.WriteLine($"Input: \"{text}\"");
            if (!text.Contains(closingChar))
            {
                //Console.WriteLine("No more tags!");
                return text;
            }
            // let's find the first occurrence of a tag...
            int tagStartIndex = text.IndexOf(openingChar);

            // OK so we know we've got a tag
            string substringOfTag = text.Substring(tagStartIndex);
            int tagEndIndex = substringOfTag.IndexOf('>');


            // Create the tag object
            string tagContent = substringOfTag.Substring(1, tagEndIndex - 1);
            string tagName = tagContent.Split(' ')[0];
            string tagArgs = substringOfTag.Substring(tagName.Length + 1, tagEndIndex - tagName.Length - 1).Trim();


            //Console.WriteLine($"TAG FOUND! name:\"{tagName}\" args:\"{tagArgs}\" at {tagStartIndex}");
            TagObject newObj = new TagObject(tagName.StartsWith(commandChar) ? tagName.Substring(1) : tagName, tagArgs, tagStartIndex, tagStartIndex);

            // add it to the list of tags

            //Console.WriteLine($"First character is {tagName[0]}");
            // is it a command tag?
            if (tagName[0] != commandChar)
            {
                //Console.WriteLine("Not a command tag");
                // It's not a command tag
                // But is it a closing tag?
                if (tagName[0] == endingChar)
                {
                    //Console.WriteLine("Closing tag");
                    // Yes it is!
                    // let's make sure it lines up with the last tag on the stack
                    TagObject lastTag = tagStack.Peek();
                    //Console.WriteLine($"Let's see... does {lastTag.Name} match with {tagName.Substring(1)}?");

                    if (lastTag.Name != tagName.Substring(1))
                    {
                        //Console.WriteLine($"ERROR AT POSITION {tagStartIndex} - TAGS NOT NESTED CORRECTLY (expected \"{lastTag.Name}\", got {tagName.Substring(1)}\")");
                        return text;
                    }

                    lastTag = tagStack.Pop();

                    // you think this is confusing?
                    // haha whatever nerd
                    lastTag.endIndex = tagStartIndex;

                    tags.Add(lastTag);

                }
                else
                {
                    //Console.WriteLine("Opening tag");
                    // If not, then we need to add it to the stack as well
                    tagStack.Push(newObj);
                }
            } else
            {
                //Console.WriteLine("COMMAND TAG");
                tags.Add(newObj);
            }


            // Parse the remaining text
            string stringBeforeTag = text.Substring(0, tagStartIndex);
            string stringAfterTag = substringOfTag.Substring(tagEndIndex + 1);

            string remaining = stringBeforeTag + stringAfterTag;

            //Console.WriteLine($"\"{stringBeforeTag}\" + \"{stringAfterTag}\"");
            //Console.WriteLine($"After the tag is removed, this string is {stringBeforeTag + stringAfterTag}");
            remaining = _InternalParseText(remaining);

            return remaining;
        }
    }

    public class TagObject
    {
        private string _name;
        private string _args;

        public int startIndex;
        public int endIndex;

        public TagObject(string name, string args, int s, int e)
        {
            _name = name;
            _args = args;
            startIndex = s;
            endIndex = e;
        }

        public override string ToString()
        {
            return $"TagObject {{ name:{_name} args:{_args} range:{startIndex}-{endIndex}}}";
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string RawArguments
        {
            get
            {
                return _args;
            }
        }

        public List<string> Arguments
        {
            get
            {
                return _args.Split(' ').ToList();
            }
        }

        public int StartIndex
        {
            get
            {
                return startIndex;
            }
        }

        public int EndIndex
        {
            get
            {
                return endIndex;
            }
        }
    }
}
