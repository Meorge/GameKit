﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit.Components.Drawing;
using GameKit.Components.Collision;
using GameKit.Assets;
using GameKit.Assets.Animation;
using GameKit.Drawing;
using System.IO;

namespace GameKit
{
    public class Engine
    {
        /// <summary>
        /// The current instance of the GameKit engine.
        /// </summary>
        public static Engine engine;

        /// <summary>
        /// A random-number generator.
        /// </summary>
        public static Random rand;

        /// <summary>
        /// If set to true, the positions, names, and rectangles for all objects will be drawn.
        /// </summary>
        public bool debugMode = false;

        public SpriteBatch spriteBatch;
        public GraphicsDeviceManager graphicsDeviceManager;
        public Viewport viewport;
        public Game game;


        // TODO: Make these lists private with public getters

        /// <summary>
        /// A list of all of the GameObjects the engine is processing.
        /// Avoid modifying this variable directly - instead, use functions like AddGameObject() and DeleteGameObject().
        /// </summary>
        public List<GameObject> gameObjects = new List<GameObject>();


        
        private List<SubEngine> subEngines = new List<SubEngine>();

        /// <summary>
        /// A list of all of the cameras the engine is processing.
        /// Avoid modifying this variable directly.
        /// </summary>
        public List<Camera> cameras = new List<Camera>();

        /// <summary>
        /// A list of all of the textures currently loaded.
        /// Avoid modifying this variable directly.
        /// </summary>
        public Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();


        /// <summary>
        /// Currently loaded assets
        /// </summary>
        public Dictionary<string, Asset> assets = new Dictionary<string, Asset>();

        // Render requests

        /// <summary>
        /// The current render request queue.
        /// Avoid modifying this variable directly - instead, use RenderRequest.Submit().
        /// </summary>
        public Queue<RenderRequest> renderRequests;

        /// <summary>
        /// Information about the current render batch.
        /// Avoid modifying this variable directly.
        /// </summary>
        public RenderBatchInfo currentRenderBatchInfo;


        public int BaseWidth = 800;
        public int BaseHeight = 480;

        public int TargetWidth
        {
            get { return graphicsDeviceManager.PreferredBackBufferWidth; }
            set { graphicsDeviceManager.PreferredBackBufferWidth = value; graphicsDeviceManager.ApplyChanges(); }
        }

        public int TargetHeight
        {
            get { return graphicsDeviceManager.PreferredBackBufferHeight;  }
            set { graphicsDeviceManager.PreferredBackBufferHeight = value; graphicsDeviceManager.ApplyChanges(); }
        }

        public float WidthRatio
        {
            get { return (float)graphicsDeviceManager.PreferredBackBufferWidth / (float)BaseWidth; }
        }

        public float HeightRatio
        {
            get { return (float)graphicsDeviceManager.PreferredBackBufferHeight / (float)BaseHeight; }
        }

        public Vector2 ScreenScale
        {
            get { return new Vector2(WidthRatio, HeightRatio); }
        }

        public Vector3 ScreenScaleVec3
        {
            get { return new Vector3(WidthRatio, HeightRatio, 1); }
        }

        public Matrix ScreenScaleMatrix
        {
            get { return Matrix.CreateScale(ScreenScaleVec3); }
        }

        /// <summary>
        /// Initializes a new GameKit engine if it has not already been initialized.
        /// </summary>
        /// <param name="g">The Game class to wrap around</param>
        public Engine(Game g)
        {
            if (engine != null)
            {
                Console.WriteLine("ERROR - Engine already exists!");
                return;
            } else
            {
                engine = this;
            }
            game = g;

            rand = new Random();

            graphicsDeviceManager = new GraphicsDeviceManager(game);

            graphicsDeviceManager.GraphicsProfile = GraphicsProfile.HiDef;
            graphicsDeviceManager.PreferMultiSampling = true;
            



            game.IsMouseVisible = true;

            game.Window.ClientSizeChanged += HandleWindowChanged;

            graphicsDeviceManager.PreferredBackBufferWidth = BaseWidth;
            graphicsDeviceManager.PreferredBackBufferHeight = BaseHeight;

            graphicsDeviceManager.PreferredBackBufferWidth *= 2;
            graphicsDeviceManager.PreferredBackBufferHeight *= 2;



        }

        void HandleWindowChanged(Object sender, EventArgs e)
        {
            graphicsDeviceManager.PreferredBackBufferWidth = game.Window.ClientBounds.Width;
            graphicsDeviceManager.PreferredBackBufferHeight = game.Window.ClientBounds.Height;

            BaseWidth = game.Window.ClientBounds.Width;
            BaseHeight = game.Window.ClientBounds.Height;

            Console.WriteLine($"Width ratio: {WidthRatio}\nHeight ratio: {HeightRatio}\n");

            Console.WriteLine($"Width: {viewport.Width}\nHeight: {viewport.Height}\n");
        }


        public void LoadAssetsFromJSON(string filePath)
        {
            string jsonData = File.ReadAllText($"./Content/{filePath}" + ".json");

            GameAssetPack assetPack = Newtonsoft.Json.JsonConvert.DeserializeObject<GameAssetPack>(jsonData);

            foreach (GameAsset asset in assetPack.assets)
            {
                if (asset.id == null) asset.id = asset.filename;

                switch (asset.type)
                {
                    case "spritefont":
                        LoadSpriteFont(asset.filename, asset.id);
                        break;
                    case "nineslice":
                        LoadNineSliceSprite(asset.filename, asset.id);
                        break;
                    case "singlesprite":
                        LoadSingleSprite(asset.filename, asset.id);
                        break;
                    case "spriteanim":
                        LoadSpriteAnimation(asset.filename, asset.id);
                        break;
                }
            }
        }


        /// <summary>
        /// Internal use only!
        /// Performs some setup.
        /// </summary>
        public void LoadContent() {
            spriteBatch = new SpriteBatch(game.GraphicsDevice);

            // setting up solid sprite
            Texture2D t = new Texture2D(Engine.engine.graphicsDeviceManager.GraphicsDevice, 1, 1);
            Color[] data = new Color[1];
            data[0] = new Color(1f, 1f, 1f, 1f);
            t.SetData(data);

            textures.Add("_solid", t);

            Sprite s = new Sprite("_solid", t.Bounds);
            //sprites.Add("_solid", s);
            assets.Add("_solid", s);

            Console.WriteLine($"SpriteBatch created - {spriteBatch}");
        }

        /// <summary>
        /// Attempts to load a texture a given path, and assign it a given ID.
        /// </summary>
        /// <param name="path">The path of the texture within the content directory</param>
        /// <param name="id">The ID to assign this texture</param>
        /// <returns>True if texture was found; false otherwise</returns>
        public bool LoadTexture(string path, string id)
        {
            Texture2D tex = LoadAsset<Texture2D>(path);
            if (tex != null && !textures.ContainsKey(id))
            {
                textures.Add(id, tex);
                return true;
            } else
            {
                return false;
            }
        }


        /// <summary>
        /// Load a sprite animation from a given path, and assigns it a given ID.
        /// </summary>
        public bool LoadSpriteAnimation(string path, string id)
        {
            SpriteAnimation anim = SpriteAnimation.FromJSON(path + ".json");
            assets.Add(id, anim);

            List<string> animSprites = anim.AllFrames();

            // finding the filepath to the sprites
            int indexOfLastSlash = path.LastIndexOf('/');
            string pathWithoutFile = path.Substring(0, indexOfLastSlash + 1);
            Console.WriteLine($"Path without file: {pathWithoutFile}");

            foreach (string s in animSprites)
            {
                LoadSingleSprite(pathWithoutFile + s, s);
            }

            return true;
        }

        /// <summary>
        /// Attempts to load a sprite font at a given path, and assigns it an ID of that path.
        /// </summary>
        /// <param name="path">The path of this sprite font within the content directory</param>
        /// <returns>True if sprite font was found; false otherwise</returns>
        public bool LoadSpriteFont(string path)
        {
            return LoadSpriteFont(path, path);
        }

        /// <summary>
        /// Attempts to load a sprite font at a given path, and assigns it a given ID.
        /// </summary>
        /// <param name="path">The path of this sprite font within the content directory</param>
        /// <param name="id">The ID to assign this sprite font</param>
        /// <returns>True if sprite font was found; false otherwise</returns>
        public bool LoadSpriteFont(string path, string id)
        {
            SpriteFont f = LoadAsset<SpriteFont>(path);
            if (f != null)
            {
                Font f_ = new Font();
                f_.spriteFont = f;
                assets.Add(id, f_);
                return true;
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Attempts to load a sprite atlas at a given path, and assigns each sprite within its given ID.
        /// </summary>
        /// <param name="s">The path of this sprite atlas.</param>
        public void LoadSpriteAtlas(string s)
        {
            // Load the sprite atlas XML and confirm it's valid
            XElement el = XElement.Load($"./Content/{s}/data.xml");
            if (el.Name != "TextureAtlas")
            {
                Console.WriteLine($"{s}.xml has invalid data - must be TextureAtlas, not {el.Name}!");
                return;
            }

            // Get the path to the sheet data and load it
            string imagePath = el.Attribute("imagePath").Value;
            bool loaded = Engine.engine.LoadTexture($"{s}/{imagePath}", s);

            // Create a new Sprite for each SubTexture element in here
            foreach (XElement e in el.Elements("SubTexture"))
            {
                Rectangle srcRect = RectangleFromXML(e);

                Sprite newSprite = new Sprite(s, srcRect);

                //Engine.engine.sprites.Add(e.Attribute("name").Value, newSprite);
                assets.Add(e.Attribute("name").Value, newSprite);
            }
        }

        /// <summary>
        /// Attempts to load a single sprite at a given path, and assigns it a given ID (or the path, if none is given).
        /// </summary>
        /// <param name="s">The path of this sprite.</param>
        /// <param name="id">The ID to assign this sprite. If no ID is given, the sprite's ID will be that of its path.</param>
        public void LoadSingleSprite(string s, string id = null)
        {
            string id_to_use = id ?? s;
            bool loaded = Engine.engine.LoadTexture(s, id_to_use);
            Sprite newSprite = new Sprite(id_to_use, Engine.engine.textures[id_to_use].Bounds);
            //Engine.engine.sprites.Add(id_to_use, newSprite);
            if (!assets.ContainsKey(id_to_use))
                assets.Add(id_to_use, newSprite);

        }


        public void LoadSingleSpriteFromRawTexture(string path, string id = null)
        {
            string id_to_use = id ?? path;
            FileStream fileStream = new FileStream(path, FileMode.Open);
            Texture2D tex = Texture2D.FromStream(Engine.engine.graphicsDeviceManager.GraphicsDevice, fileStream);
            fileStream.Dispose();

            textures.Add(id_to_use, tex);
            Sprite newSprite = new Sprite(id_to_use, Engine.engine.textures[id_to_use].Bounds);
            //Engine.engine.sprites.Add(id_to_use, newSprite);
            assets.Add(id_to_use, newSprite);
        }

        /// <summary>
        /// Attempts to load a nine-slice sprite at a given path, and assigns it a given ID (or the path, if none is given).
        /// </summary>
        /// <param name="s">The path of this nine-slice sprite.</param>
        /// <param name="id">The ID to assign this nine-slice sprite. If no ID is given, the nine-slice sprite's ID will be that of its path.</param>
        public void LoadNineSliceSprite(string s, string id = null)
        {
            string id_to_use = id ?? s;
            NineSliceSprite nineSlice = new NineSliceSprite();
            nineSlice.sourceTexture = s;

            // Load the sprite atlas XML and confirm it's valid
            XElement el = XElement.Load($"./Content/{s}/data.xml");
            if (el.Name != "NineSlice")
            {
                Console.WriteLine($"{s}.xml has invalid data - must be TextureAtlas, not {el.Name}!");
                return;
            }

            // Get the path to the sheet data and load it
            string imagePath = el.Attribute("imagePath").Value;
            bool loaded = Engine.engine.LoadTexture($"{s}/{imagePath}", s);


            int padding = 0;// int.Parse(el.Attribute("allPadding").Value);
            Console.WriteLine($"for {imagePath}, el.Attribute(\"allPadding\") = {el.Attribute("allPadding")}");
            if ((string)el.Attribute("allPadding") != null)
            {
                Console.WriteLine($"cool allPadding exists on {imagePath}, it's {el.Attribute("allPadding").Value}");
                padding = int.Parse(el.Attribute("allPadding").Value);
            }
            else Console.WriteLine($"allPadding isn't on this one ({imagePath}), so padding should remain zero {padding}");


            int topPadding = padding;
            int leftPadding = padding;
            int rightPadding = padding;
            int bottomPadding = padding;

            if (el.Attribute("topPadding") != null)
                topPadding = int.Parse(el.Attribute("topPadding").Value);

            if (el.Attribute("leftPadding") != null)
                leftPadding = int.Parse(el.Attribute("leftPadding").Value);

            if (el.Attribute("rightPadding") != null)
                rightPadding = int.Parse(el.Attribute("rightPadding").Value);

            if (el.Attribute("bottomPadding") != null)
                bottomPadding = int.Parse(el.Attribute("bottomPadding").Value);

            nineSlice.topPadding = topPadding;
            nineSlice.leftPadding = leftPadding;
            nineSlice.rightPadding = rightPadding;
            nineSlice.bottomPadding = bottomPadding;

            Console.WriteLine($"Nine slice added with key {id_to_use}, paddings are {topPadding} {leftPadding} {rightPadding} {bottomPadding}");
            //sprites.Add(id_to_use, nineSlice);
            assets.Add(id_to_use, nineSlice);

        }

        /// <summary>
        /// Parses an XElement with x, y, width, and height attributes into a Rectangle object.
        /// </summary>
        /// <param name="e">The XElement to parse.</param>
        /// <returns>The Rectangle object with the XElement's given x, y, width, and height attributes</returns>
        public Rectangle RectangleFromXML(XElement e)
        {
            return new Rectangle(
                    int.Parse(e.Attribute("x").Value),
                    int.Parse(e.Attribute("y").Value),
                    int.Parse(e.Attribute("width").Value),
                    int.Parse(e.Attribute("height").Value)
                );
        }

        /// <summary>
        /// Loads an asset of the given type and with the given name/path.
        /// </summary>
        /// <typeparam name="T">The type of asset to load.</typeparam>
        /// <param name="name">The name/path of the asset to load.</param>
        /// <returns>The asset loaded, if found.</returns>
        public T LoadAsset<T>(string name)
        {
            Console.WriteLine($"Attempting to load asset <{name}> from directory <{game.Content.RootDirectory}>");


            return game.Content.Load<T>(name);
        }

        /// <summary>
        /// Sets the visibility of the mouse cursor when over the game window.
        /// </summary>
        /// <param name="v">Visibility of mouse cursor</param>
        /// <returns>Reference to this Engine</returns>
        public Engine SetMouseVisible(bool v)
        {
            game.IsMouseVisible = v;
            return this;
        }

        /// <summary>
        /// Adds a given GameObject and all of its children to this Engine's GameObject hierarchy. This function is recursive - multiple layers of children can be added at once.
        /// </summary>
        /// <param name="o">GameObject to add</param>
        /// <returns>Reference to this Engine</returns>
        public Engine AddGameObject(GameObject o)
        {
            if (gameObjects.Contains(o))
            {
                Console.WriteLine($"Engine - GameObject with name \"{o.name}\" has already been added to the scene");
                return this;
            }

            gameObjects.Add(o);
            foreach (GameObject child in o.Children)
            {
                AddGameObject(child);
            }
            return this;
        }

        /// <summary>
        /// Deletes a given GameObject (and maybe its children??? idk)
        /// </summary>
        /// <param name="o">GameObject to delete</param>
        /// <returns>Reference to this Engine</returns>
        public Engine DeleteGameObject(GameObject o)
        {
            gameObjects.Remove(o);
            o.Dispose();
            return this;
        }

        public void ExitGame()
        {
            game.Exit();
        }

        public class SubEngine
        {
            public Component comp;
            public int index = 1;

            public SubEngine(Component c, int i)
            {
                comp = c;
                index = i;
            }
        }

        public Engine AddSubEngine<T>(int i=0) where T: Component
        {
            T newComp = Activator.CreateInstance<T>();
            SubEngine newSubEngine = new SubEngine(newComp, i);

            subEngines.Add(newSubEngine);

            subEngines.Sort((a, b) => a.index.CompareTo(b.index));

            return this;
        }

        public T GetSubEngine<T>() where T : Component
        {
            foreach (SubEngine comp in subEngines)
            {
                if (comp.comp is T)
                {
                    return (T)comp.comp;
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the size of the game window.
        /// </summary>
        /// <param name="w">New width of the game window</param>
        /// <param name="h">New height of the game window</param>
        /// <returns>Reference to this Engine</returns>
        public Engine SetWindowSize(int w, int h)
        {
            graphicsDeviceManager.PreferredBackBufferWidth = w;
            graphicsDeviceManager.PreferredBackBufferHeight = h;
            graphicsDeviceManager.ApplyChanges();
            return this;
        }

        /// <summary>
        /// Scales the current size of the game window by a certain amount.
        /// </summary>
        /// <param name="w">Amount to scale game window width by</param>
        /// <param name="h">Amount to scale game window height by</param>
        /// <returns>Reference to this Engine</returns>
        public Engine ScaleWindowSize(float w, float h)
        {
            SetWindowSize((int)(graphicsDeviceManager.PreferredBackBufferWidth * w),
                (int)(graphicsDeviceManager.PreferredBackBufferHeight * h));
            return this;
        }

       
        /// <summary>
        /// For internal use only!
        /// Updates all of the registered components, input, tweening, etc
        /// </summary>
        /// <param name="gT"></param>
        public void Update(GameTime gT)
        {
            // keep the scale stuff
            
            SortGameObjects();

            // subengines
            List<SubEngine> preSubengines = subEngines.FindAll((a) => a.index < 0);
            List<SubEngine> postSubengines = subEngines.FindAll((a) => a.index >= 0);

            foreach (SubEngine subEngine in preSubengines)
            {
                subEngine.comp.Update(gT);
            }

            InputManagement.Input.Update();
            Tweening.TweenManager.Update(gT);

            List<Timer> tempTimerList = new List<Timer>(Timer.timers);

            foreach (Timer t in tempTimerList)
            {
                t.Update(gT);
            }

            Collider.CheckForCollisions();

            List<GameObject> currentGameObjects = new List<GameObject>(gameObjects);
            foreach (GameObject g in currentGameObjects)
            {
                g.Update(gT);
            }


            foreach (SubEngine subEngine in postSubengines)
            {
                subEngine.comp.Update(gT);
            }
        }

        /// <summary>
        /// For internal use only!
        /// Gets the highest priority camera
        /// </summary>
        /// <returns></returns>
        public Camera GetActiveCamera()
        {
            Camera highestPriorityCamera = null;
            foreach (Camera c in cameras)
            {
                if (highestPriorityCamera == null || highestPriorityCamera.priority < c.priority)
                {
                    highestPriorityCamera = c;
                }
            }
            return highestPriorityCamera;
        }

        /// <summary>
        /// For internal use only!
        /// Calculates the ZRange values for game objects.
        /// </summary>
        public void SortGameObjects()
        {
            gameObjects.Sort((a, b) => a.CumulativeZ.CompareTo(b.CumulativeZ));

            foreach (GameObject o in gameObjects)
            {
                o.UpdateZRange();
            }

            //Console.WriteLine("GameObjects sorted!");
            //Console.WriteLine("----------");
            //Console.WriteLine("FIRST TO DRAW");
            //foreach (GameObject g in gameObjects)
            //{
            //    Console.WriteLine($"{g.name} ({g.CumulativeZ})");
            //}
            //Console.WriteLine("LAST TO DRAW");
            //Console.WriteLine("----------");

        }

        /// <summary>
        /// For internal use only!
        /// Performs all render requests, and debug drawing.
        /// </summary>
        /// <param name="gT"></param>
        public void Draw(GameTime gT)
        {
            game.GraphicsDevice.Clear(Color.Black);

            

            // figure out the active camera
            Camera cam = GetActiveCamera();

            viewport = game.GraphicsDevice.Viewport;

            renderRequests = new Queue<RenderRequest>();
            
            foreach (GameObject g in gameObjects)
            {
                g.Draw(gT, cam);
            }

            currentRenderBatchInfo = RenderBatchInfo.Default();

            if (cam == null)
            {
                Console.WriteLine("ERROR - No camera set up!!");
                return;
            }

            RasterizerState rast = new RasterizerState { MultiSampleAntiAlias = true };
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicWrap, DepthStencilState.DepthRead, rast, null, transformMatrix: cam.GetMatrix());

            while (renderRequests.Count > 0)
            {
                RenderRequest r = renderRequests.Dequeue();

                RenderBatchInfo thisBatchInfo = r.batchInfo;
                    
                if (currentRenderBatchInfo != thisBatchInfo)
                {
                    spriteBatch.End();
                    currentRenderBatchInfo = thisBatchInfo;

                    
                    spriteBatch.Begin(SpriteSortMode.Deferred,
                        currentRenderBatchInfo.blendState,
                        currentRenderBatchInfo.samplerState,
                        DepthStencilState.DepthRead,
                        RasterizerState.CullNone,
                        currentRenderBatchInfo.effect,
                        currentRenderBatchInfo.matrix);

                }
                r.Draw(ref spriteBatch);
            }

            spriteBatch.End();

            if (!debugMode) return;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicWrap, DepthStencilState.DepthRead, RasterizerState.CullNone, null);
            Texture2D t = new Texture2D(Engine.engine.graphicsDeviceManager.GraphicsDevice, 1, 1);
            Color[] data = new Color[1];



            foreach (GameObject obj in gameObjects)
            {
                Random randSeeded = new Random(obj.GetHashCode());
                data[0] = new Color(0f, 1f, 0f, 0.1f);
                t.SetData(data);

                int x = (int)obj.AbsolutePosition.X;
                int y = (int)obj.AbsolutePosition.Y;
                spriteBatch.Draw(t, obj.AbsoluteRectangle, Color.Green);

            }

            foreach (GameObject obj in gameObjects) {
                int x = (int)obj.AbsolutePosition.X;
                int y = (int)obj.AbsolutePosition.Y;
                spriteBatch.Draw(t, new Rectangle(x - 5, y - 5, 10, 10), Color.Red);
                if (assets.ContainsKey("Font"))
                    spriteBatch.DrawString(((Font)assets["Font"]).spriteFont, obj.name, new Vector2(x, y), Color.Yellow, 0f, Vector2.Zero, 0.6f, SpriteEffects.None, 0f);
                else
                {
                    Console.WriteLine("There is no font named 'Font' loaded, so game objects' names won't be drawn.");
                }
            }
            spriteBatch.End();
        }
    }
}
