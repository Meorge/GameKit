﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit.Assets;

namespace GameKit.Drawing
{
    public struct RenderBatchInfo
    {
        // spritebatch begin info
        public BlendState blendState;
        public SamplerState samplerState;
        public Effect effect;
        public Matrix matrix;

        public static RenderBatchInfo Default()
        {
            RenderBatchInfo b = new RenderBatchInfo();
            b.blendState = BlendState.AlphaBlend;
            b.samplerState = SamplerState.AnisotropicWrap;
            b.effect = null;
            b.matrix = Matrix.Identity;
            return b;
        }

        private static bool Equals(RenderBatchInfo l, RenderBatchInfo r)
        {
            return (l.blendState == r.blendState) &&
                    (l.samplerState == r.samplerState) &&
                    (l.effect == r.effect) &&
                    (l.matrix == r.matrix);
        }

        public static bool operator ==(RenderBatchInfo l, RenderBatchInfo r) {
            return Equals(l, r);
        }

        public static bool operator !=(RenderBatchInfo l, RenderBatchInfo r)
        {
            return !Equals(l, r);
        }
    }

    public class RenderRequest
    {
        public RenderBatchInfo batchInfo;

        public enum RenderRequestType
        {
            Sprite = 0,
            Text = 1
        }

        public RenderRequestType type = RenderRequestType.Sprite;

        // draw info
        public string sprite;
        
        public Rectangle destRectangle;
        public Rectangle sourceRectangle;
        public Color tintColor;
        public float rotation;
        public Vector2 center;
        public SpriteEffects spriteEffects;
        public float z;

        public string fontID;
        public string text = "Sample text";
        public Vector2 scale;

        public RenderRequest()
        {
        }

        public RenderRequest SetBatchInfo(BlendState bS, SamplerState sS, Effect ef, Matrix? m)
        {
            if (m == null) m = Matrix.Identity;

            batchInfo = new RenderBatchInfo();
            batchInfo.blendState = bS;
            batchInfo.samplerState = sS;
            batchInfo.effect = ef;
            batchInfo.matrix = (Matrix)m;
            return this;
        }

        public RenderRequest SetRenderType(RenderRequestType t)
        {
            type = t;
            return this;
        }

        public RenderRequest SetText(string t)
        {
            text = t;
            return this;
        }

        public RenderRequest SetDrawTextInfo(string _fontID, string _textString, Vector2 _pos, Color _color, float _rotation, Vector2 _center, Vector2 _scale, SpriteEffects fx, float _z)
        {
            type = RenderRequestType.Text;
            fontID = _fontID;
            text = _textString;
            tintColor = _color;
            rotation = _rotation;
            center = _center;

            destRectangle = new Rectangle((int)_pos.X, (int)_pos.Y, 0, 0);
            scale = _scale;
            spriteEffects = fx;
            z = _z;
            
            return this;
        }

        public RenderRequest SetDrawSpriteInfo(string sID, Rectangle dR, Color tC, float r, Vector2 c, SpriteEffects sE, float _z, Rectangle sR = new Rectangle())
        {
            type = RenderRequestType.Sprite;
            sprite = sID;
            destRectangle = dR;
            tintColor = tC;
            rotation = r;
            center = c;
            spriteEffects = sE;
            z = _z;
            sourceRectangle = sR;
            return this;
        }

        public void Submit()
        {
            Engine.engine.renderRequests.Enqueue(this);
        }

        public void Draw(ref SpriteBatch sB)
        {
            if (type == RenderRequestType.Sprite) DrawSprite(ref sB);
            else if (type == RenderRequestType.Text) DrawText(ref sB);
        }


        private void DrawSprite(ref SpriteBatch sB) {
            Rectangle sourceRect;

            var s = (AbstractSprite)Engine.engine.assets[sprite];

            Texture2D spriteTex = Engine.engine.textures[s.sourceTexture];

            //Console.WriteLine($"Attempting to load sprite with ID {sprite}");
            if (sourceRectangle.IsEmpty) sourceRect = ((Sprite)Engine.engine.assets[sprite]).sourceRectangle;
            else sourceRect = sourceRectangle;


            //Vector2 pos = new Vector2(destRectangle.X, destRectangle.Y);
            //pos = Vector2.Transform(pos, Engine.engine.ScreenScaleMatrix);

            //Vector2 dim = new Vector2(destRectangle.Width, destRectangle.Height);
            //dim = Vector2.Transform(pos, Engine.engine.ScreenScaleMatrix);

            //destRectangle.X = (int)pos.X;
            //destRectangle.Y = (int)pos.Y;
            //destRectangle.X = (int)(destRectangle.X * Engine.engine.WidthRatio);
            //destRectangle.Y = (int)(destRectangle.Y * Engine.engine.HeightRatio);
            //destRectangle.Width = (int)(destRectangle.Width * Engine.engine.WidthRatio);
            //destRectangle.Height = (int)(destRectangle.Height * Engine.engine.HeightRatio);

            sB.Draw(
                spriteTex,
                destRectangle,
                sourceRect,
                tintColor,
                rotation,
                center,
                spriteEffects,
                z
            );
        }

        private void DrawText(ref SpriteBatch sB)
        {
            SpriteFont spriteFont = ((Font)Engine.engine.assets[fontID]).spriteFont;
            sB.DrawString(
                spriteFont,
                text,
                destRectangle.Location.ToVector2(),
                tintColor,
                rotation,
                center,
                scale,
                spriteEffects,
                z
            );
        }
    }
}
