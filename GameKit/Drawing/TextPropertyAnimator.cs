﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Drawing
{
    public static class TextPropertyAnimator
    {
        public static TextProperty FadeIn(float textAnimateAmount, Vector2 from)
        {
            float amt = MathHelper.Clamp(textAnimateAmount, 0f, 1f);
            TextProperty prop = TextProperty.White;

            prop.color.A = (byte)(amt * 255);

            prop.offset = new Vector2(
                MathHelper.Lerp(from.X, 0, amt),
                MathHelper.Lerp(from.Y, 0, amt)
                );

            return prop;
        }

        public static TextProperty FadeOut(float textAnimateAmount)
        {
            float amt = MathHelper.Clamp(textAnimateAmount, 0f, 1f);
            TextProperty prop = TextProperty.White;

            prop.color.A = (byte)(255 - (amt * 255));

            prop.offset = new Vector2(
                0f,
                MathHelper.Lerp(0, -8, amt)
                );

            return prop;
        }
    }
}
