﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Drawing
{
    public struct TextProperty
    {
        public static TextProperty White
        {
            get
            {
                return new TextProperty(Vector2.Zero, Color.White);
            }
        }

        public static TextProperty Invisible
        {
            get
            {
                return new TextProperty(Vector2.Zero, new Color(1f, 1f, 1f, 0f));
            }
        }

        public Vector2 offset;
        public Color color;

        public TextProperty(Vector2 of, Color col)
        {
            offset = of;
            color = col;
        }
    }
}
