﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;


namespace GameKit.Tweening
{
    // Mmmkay so here's how this is gonna work
    // So, the Engine has a list of tween sequences that have been pushed
    // The engine will evaluate every single tween sequence
    // FOR EACH SEQUENCE:
    //      The engine will determine what the current TweenGroup is, and evaluate it
    //      FOR EACH TWEEN GROUP:
    //              The engine will add the current gameTime to the counter.
    //              Then, it will check to see if all of the tweens are at max.
    //              If so, that group will be marked completed and move onto the next


    public static class TweenManager
    {
        public static List<TweenSequence> sequences = new List<TweenSequence>();

        public static void Update(GameTime gT)
        {
            float time = (float)gT.ElapsedGameTime.TotalSeconds;

            List<TweenSequence> seqs = new List<TweenSequence>(sequences);
            foreach (TweenSequence s in seqs)
            {
                s.Update(time);
            }
        }

        public static void DeleteAllSequencesWithName(string name)
        {
            sequences.RemoveAll((a) => a.name == name && !a.Started);
        }
    }

    public class TweenSequence : IDisposable
    {
        private bool _disposed = false;
        public List<TweenGroup> tweenGroups;

        private bool _repeat = false;
        public string name = "";

        public bool Complete
        {
            get
            {
                if (_disposed)
                {
                    Console.WriteLine("ERROR - Trying to see if a disposed TweenSequence is complete, returning false");
                    return false;
                }
                foreach (TweenGroup t in tweenGroups)
                {
                    if (!t.Complete) return false;
                }
                return true;
            }
        }

        public bool Started
        {
            get
            {
                return TweenManager.sequences.Contains(this);
            }
        }

        public TweenSequence()
        {
            Construct();
        }

        public TweenSequence(bool repeat = false)
        {
            _repeat = repeat;
            Construct();
        }

        public TweenSequence(string id)
        {
            name = id;
            Construct();
        }

        public TweenSequence(string id = "", bool repeat = false)
        {
            name = id;
            _repeat = repeat;
            Construct();
        }



        void Construct()
        {
            tweenGroups = new List<TweenGroup>();
            
        }

        public TweenSequence Start()
        {
            TweenManager.sequences.Add(this);
            return this;
        }

        public TweenSequence Then(Tween tween)
        {
            if (_disposed) return null;
            tweenGroups.Add(new TweenGroup(tween));
            return this;
        }

        public TweenSequence And(Tween tween)
        {
            if (_disposed) return null;
            tweenGroups[tweenGroups.Count - 1].tweens.Add(tween);
            return this;
        }

        public void Update(float time)
        {
            if (_disposed) return;
            TweenGroup groupToEvaluate = GetFirstUnfinishedTweenGroup();
            if (groupToEvaluate != null) groupToEvaluate.Update(time);
            else
            {
                if (!_repeat)
                {
                    this.Dispose();
                    return;
                }
                else foreach (TweenGroup g in tweenGroups) g.Reset();
            }
        }

        public TweenGroup GetFirstUnfinishedTweenGroup()
        {
            foreach (TweenGroup t in tweenGroups)
            {
                if (!t.Complete) return t;
            }
            return null;
        }

        public void Stop()
        {
            Dispose();
        }

        public void Dispose()
        {
            TweenManager.sequences.Remove(this);

            foreach (TweenGroup g in tweenGroups)
            {
                g.Dispose();
            }
            tweenGroups = null;
            _disposed = true;
        }
    }

    public class TweenGroup : IDisposable
    {
        private bool _disposed = false;
        public List<Tween> tweens;

        public TweenGroup(Tween t)
        {
            tweens = new List<Tween>();
            tweens.Add(t);
        }

        public bool Complete
        {
            get
            {
                if (_disposed)
                {
                    Console.WriteLine("ERROR - Trying to see if a disposed TweenGroup is complete, returning false");
                    return false;
                }
                foreach (Tween t in tweens)
                {
                    if (!t.Complete) return false;
                }
                return true;
            }
        }

        public void Update(float time)
        {
            foreach (Tween t in tweens)
            {
                if (!t.Complete) t.Update(time);
            }
        }

        public void Reset()
        {
            foreach (Tween t in tweens) t.Reset();
        }

        public void Dispose()
        {
            foreach (Tween tw in tweens)
            {
                tw.Dispose();
            }
            tweens = null;
            _disposed = true;
        }
    }


    public class Tween : IDisposable
    {
        private string name = "";
        private bool _disposed = false;

        private bool _initialSet = false;
        private bool _onCompleteCalled = false;
        private float _startValue;
        private float _endValue;
        private float _duration;

        private Ease _ease;

        private ConstrainedValue _delayOffset;
        private ConstrainedValue _currentTimeFraction;

        public delegate float TweenGetter();
        public delegate void TweenSettter(float value);

        public delegate void TweenComplete();


        private TweenGetter _getter;
        private TweenSettter _setter;
        private TweenComplete _onComplete;

        public bool Complete
        {
            get
            {
                if (_currentTimeFraction.Minimum == 0f && _currentTimeFraction.Maximum == 0f)
                {
                    return _onCompleteCalled;
                }
                return _currentTimeFraction.IsFull;
            }
        }

        public Tween(float endValue, TweenGetter getter, TweenSettter setter, float duration, Ease ease = Ease.Linear, float offset = 0f, TweenComplete onComplete = null)
        {
            _endValue = endValue;
            _duration = duration;

            _getter = getter;
            _setter = setter;

            _ease = ease;

            _currentTimeFraction = new ConstrainedValue(0f, 0f, true);
            _delayOffset = new ConstrainedValue(offset);
            _currentTimeFraction.SetMax(_duration);

            _onComplete = onComplete;
        }

        public Tween(float endValue, TweenGetterSetter getterSetter, float duration, Ease ease = Ease.Linear, float offset = 0f, TweenComplete onComplete = null) :
            this(endValue, getterSetter.Getter, getterSetter.Setter, duration, ease, offset, onComplete)
        { }


        public static Tween To(float endValue, TweenGetter getter, TweenSettter setter, float duration, Ease ease = Ease.Linear, float offset = 0f, TweenComplete onComplete = null)
        {
            return new Tween(endValue, getter, setter, duration, ease, offset, onComplete);
        }

        public static Tween To(float endValue, TweenGetterSetter getterSetter, float duration, Ease ease = Ease.Linear, float offset = 0f, TweenComplete onComplete = null)
        {
            return new Tween(endValue, getterSetter, duration, ease, offset, onComplete);
        }

        public static Tween Wait(float duration, TweenComplete onComplete = null)
        {
            return new Tween(0f, null, null, duration, Ease.Linear, 0f, onComplete);
        }

        public static Tween Call(TweenComplete function)
        {
            return Wait(0f, function);
        }

        public Tween SetName(string n)
        {
            name = n;
            return this;
        }

        public void Reset()
        {
            //Console.WriteLine($"Resetting tween with name \"{name}\"");
            _currentTimeFraction.ResetToMin();
            _initialSet = false;
            _onCompleteCalled = false;
            _delayOffset.ResetToMax();
        }

        public void Update(float time)
        {
            if (_disposed) return;

            if (_currentTimeFraction.IsFull && !_onCompleteCalled && _onComplete != null)
            {
                _onComplete();
                _onCompleteCalled = true;
                return;
            }

            if (!_delayOffset.IsEmpty)
            {
                _delayOffset.DecrementBy(time);
                return;
            }

            if (_currentTimeFraction.IsEmpty && !_initialSet && _getter != null)
            {
                _initialSet = true;
                _startValue = _getter();
                //Console.WriteLine($"TWEEN - Starting out value is {_startValue}");
            }

            _currentTimeFraction.IncrementBy(time);
            //Console.WriteLine($"TWEEN - {time} passed, fraction is now {_currentTimeFraction.Percentage}");

            // TODO: Add more easing functions, not just lerp
            float easedValue = EasingFunctions.GetEasingFunction(_ease)(_currentTimeFraction.Percentage);
            float newValue = MathHelper.Lerp(_startValue, _endValue, easedValue);

            //if (_getter != null) Console.WriteLine($"TWEEN - getter returned {_getter()}, new value is {newValue}");
            //if (_setter == null) Console.WriteLine("TWEEN - setter is null");
            //Console.WriteLine($"TWEEN - ===========");
            _setter?.Invoke(newValue);


        }

        public void Dispose()
        {
            _currentTimeFraction = null;
            _getter = null;
            _setter = null;

            _disposed = true;
            //Console.WriteLine($"Disposing of tween with name \"{name}\"");
        }
    }

    public class TweenGetterSetter
    {
        private Tween.TweenGetter _getter;
        private Tween.TweenSettter _setter;

        public TweenGetterSetter(Tween.TweenGetter getter, Tween.TweenSettter setter)
        {
            _getter = getter;
            _setter = setter;
        }

        public Tween.TweenGetter Getter
        {
            get
            {
                return _getter;
            }
        }

        public Tween.TweenSettter Setter
        {
            get
            {
                return _setter;
            }
        }
    }
}
