﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Tweening
{
    public enum Ease
    {
        Linear,
        EaseInSine,
        EaseOutSine,
        EaseInOutSine,
        EaseInQuad,
        EaseOutQuad,
        EaseInOutQuad,
        EaseInCubic,
        EaseOutCubic,
        EaseInOutCubic,
        EaseInQuart,
        EaseOutQuart,
        EaseInOutQuart,
        EaseInQuint,
        EaseOutQuint,
        EaseInOutQuint,
        EaseInExpo,
        EaseOutExpo,
        EaseInOutExpo,
        EaseInCirc,
        EaseOutCirc,
        EaseInOutCirc,
        EaseInBack,
        EaseOutBack,
        EaseInOutBack,
        EaseInElastic,
        EaseOutElastic,
        EaseInOutElastic,
        EaseInBounce,
        EaseOutBounce,
        EaseInOutBounce
    }

    public static class EasingFunctions
    {
        // Easing functions are from the awesome website https://easings.net/#
        // my hands hurt

        public delegate float EasingFunction(float t);

        public static EasingFunction GetEasingFunction(Ease e)
        {
            switch (e)
            {
                case Ease.EaseInSine: return EaseInSine;
                case Ease.EaseOutSine: return EaseOutSine;
                case Ease.EaseInOutSine: return EaseInOutSine;
                case Ease.EaseInQuad: return EaseInQuad;
                case Ease.EaseOutQuad: return EaseOutQuad;
                case Ease.EaseInOutQuad: return EaseInOutQuad;
                case Ease.EaseInCubic: return EaseInCubic;
                case Ease.EaseOutCubic: return EaseOutCubic;
                case Ease.EaseInOutCubic: return EaseInOutCubic;
                case Ease.EaseInQuart: return EaseInQuart;
                case Ease.EaseOutQuart: return EaseOutQuart;
                case Ease.EaseInOutQuart: return EaseInOutQuart;
                case Ease.EaseInQuint: return EaseInQuint;
                case Ease.EaseOutQuint: return EaseOutQuint;
                case Ease.EaseInOutQuint: return EaseInOutQuint;
                case Ease.EaseInExpo: return EaseInExpo;
                case Ease.EaseOutExpo: return EaseOutExpo;
                case Ease.EaseInOutExpo: return EaseInOutExpo;
                case Ease.EaseInCirc: return EaseInCirc;
                case Ease.EaseOutCirc: return EaseOutCirc;
                case Ease.EaseInOutCirc: return EaseInOutCirc;
                case Ease.EaseInBack: return EaseInBack;
                case Ease.EaseOutBack: return EaseOutBack;
                case Ease.EaseInOutBack: return EaseInOutBack;
                case Ease.EaseInElastic: return EaseInElastic;
                case Ease.EaseOutElastic: return EaseOutElastic;
                case Ease.EaseInOutElastic: return EaseInOutElastic;
                case Ease.EaseInBounce: return EaseInBounce;
                case Ease.EaseOutBounce: return EaseOutBounce;
                case Ease.EaseInOutBounce: return EaseInOutBounce;
                default: return Linear;
            }
        }
        private static readonly float c1 = 1.70158f;
        private static readonly float c2 = c1 * 1.525f;
        private static readonly float c3 = c1 + 1;
        private static readonly float c4 = (2 * MathF.PI) / 3f;
        private static readonly float c5 = (2 * MathF.PI) / 4.5f;

        private static readonly float n1 = 7.5625f;
        private static readonly float d1 = 2.75f;

        public static float Linear(float t)
        {
            return t; // the best function
        }

        public static float EaseInSine(float t)
        {
            return 1 - MathF.Cos((t * MathF.PI) / 2f);
        }

        public static float EaseOutSine(float t)
        {
            return MathF.Sin((t * MathF.PI) / 2f);
        }

        public static float EaseInOutSine(float t)
        {
            return -(MathF.Cos(MathF.PI * t) - 1) / 2;
        }

        public static float EaseInQuad(float t)
        {
            return t * t;
        }

        public static float EaseOutQuad(float t)
        {
            return 1 - (1 - t) * (1 - t);
        }

        public static float EaseInOutQuad(float t)
        {
            return t < 0.5f ?
                2 * EaseInQuad(t)
                :
                1 - MathF.Pow(-2 * t + 2, 2) / 2
            ;
        }

        public static float EaseInCubic(float t)
        {
            return t * t * t;
        }

        public static float EaseOutCubic(float t)
        {
            return 1 - MathF.Pow(1 - t, 3);
        }

        public static float EaseInOutCubic(float t)
        {
            return t < 0.5f ?
                4 * EaseInCubic(t)
                :
                1 - MathF.Pow(-2 * t + 2, 3) / 2
                ;
        }

        public static float EaseInQuart(float t)
        {
            return t * t * t * t;
        }

        public static float EaseOutQuart(float t)
        {
            return 1 - MathF.Pow(1 - t, 4);
        }

        public static float EaseInOutQuart(float t)
        {
            return t < 0.5f ?
                8 * EaseInQuart(t)
                :
                1 - MathF.Pow(-2 * t + 2, 4) / 2
            ;
        }

        public static float EaseInQuint(float t)
        {
            return t * t * t * t * t;
        }

        public static float EaseOutQuint(float t)
        {
            return 1 - MathF.Pow(1 - t, 5);
        }

        public static float EaseInOutQuint(float t)
        {
            return t < 0.5f ?
                16 * EaseInQuint(t)
                :
                1 - MathF.Pow(-2 * t + 2, 5) / 2
                ;
        }

        public static float EaseInExpo(float t)
        {
            return t == 0 ?
                0
                :
                MathF.Pow(2, 10 * t - 10)
            ;
        }

        public static float EaseOutExpo(float t)
        {
            return t == 1 ?
                1
                :
                1 - MathF.Pow(2, -10 * t)
            ;
        }

        public static float EaseInOutExpo(float t)
        {
            return t == 0 ?
                0
                :
                t == 1 ?
                    1
                    :
                    t < 0.5f ?
                        MathF.Pow(2, 20 * t - 10) / 2
                        :
                        (2 - MathF.Pow(2, -20 * t + 10)) / 2
            ;
        }

        public static float EaseInCirc(float t)
        {
            return 1 - MathF.Sqrt(1 - MathF.Pow(t, 2));
        }

        public static float EaseOutCirc(float t)
        {
            return MathF.Sqrt(1 - MathF.Pow(t - 1, 2));
        }

        public static float EaseInOutCirc(float t)
        {
            return t < 0.5f ?
                (1 - MathF.Sqrt(1 - MathF.Pow(2 * t, 2))) / 2
                :
                (MathF.Sqrt(1 - MathF.Pow(-2 * t + 2, 2)) + 1) / 2
                ;
        }

        public static float EaseInBack(float t)
        {
            return c3 * EaseInCubic(t) - c1 * EaseInQuad(t);
        }

        public static float EaseOutBack(float t)
        {
            return 1 + c3 * MathF.Pow(t - 1, 3) + c1 * MathF.Pow(t - 1, 2);
        }

        public static float EaseInOutBack(float t)
        {
            return t < 0.5f ?
                (MathF.Pow(2 * t, 2) * ((c2 + 1) * 2 * t - c2)) / 2
                :
                (MathF.Pow(2 * t - 2, 2) * ((c2 + 1) * (t * 2 - 2) + c2) + 2) / 2
            ;
        }

        public static float EaseInElastic(float t)
        {
            return t == 0 ?
                0
                :
                t == 1 ?
                    1
                    :
                    -MathF.Pow(2, 10 * t - 10) * MathF.Sin((t * 10 - 10.75f) * c4)
            ;
        }

        public static float EaseOutElastic(float t)
        {
            return t == 0 ?
                0
                :
                t == 1 ?
                    1
                    :
                    MathF.Pow(2, -10 * t) * MathF.Sin((t * 10 - 0.75f) * c4) + 1
            ;
        }

        public static float EaseInOutElastic(float t)
        {
            return t == 0 ?
                0
                :
                t == 1 ?
                    1
                    :
                    t < 0.5f ?
                        -(MathF.Pow(2, 20 * t - 10) * MathF.Sin((20 * t - 11.125f) * c5)) / 2
                        :
                        (MathF.Pow(2, -20 * t + 10) * MathF.Sin((20 * t - 11.125f) * c5)) / 2 + 1
            ;
        }

        public static float EaseInBounce(float t)
        {
            return 1 - EaseOutBounce(1 - t);
        }

        public static float EaseOutBounce(float t)
        {
            if (t < 1 / d1) return n1 * EaseInQuad(t);
            else if (t < 2 / d1) return n1 * (t -= 1.5f / d1) * t + 0.75f;
            else if (t < 2.5f / d1) return n1 * (t -= 2.25f / d1) * t + 0.9375f;
            else return n1 * (t -= 2.625f / d1) * t + 0.984375f;
        }

        public static float EaseInOutBounce(float t)
        {
            return t < 0.5f ?
                (1 - EaseOutBounce(1 - 2 * t)) / 2f
                :
                (1 + EaseOutBounce(2 * t - 1)) / 2f
                ;
        }
    }
}
