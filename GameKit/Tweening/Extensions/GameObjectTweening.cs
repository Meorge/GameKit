﻿using System;
namespace GameKit.Tweening
{
    public static class GameObjectTweening
    {
        public static TweenGetterSetter GetTweenX (this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.X; },
                (a) => { obj.X = (int)a; }
            );
        }

        public static TweenGetterSetter GetTweenY(this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.Y; },
                (a) => { obj.Y = (int)a; }
            );
        }

        public static TweenGetterSetter GetTweenWidth(this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.Width; },
                (a) => { obj.Width = (int)a; }
            );
        }

        public static TweenGetterSetter GetTweenHeight(this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.Height; },
                (a) => { obj.Height = (int)a; }
            );
        }

        public static TweenGetterSetter GetTweenRotation(this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.Rotation; },
                (a) => { obj.Rotation = a; }
            );
        }

        public static TweenGetterSetter GetTweenXScale(this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.scale.X; },
                (a) => { obj.scale.X = a; }
            );
        }

        public static TweenGetterSetter GetTweenYScale(this GameObject obj)
        {
            return new TweenGetterSetter(
                () => { return obj.scale.Y; },
                (a) => { obj.scale.Y = a; }
            );
        }
    }
}
