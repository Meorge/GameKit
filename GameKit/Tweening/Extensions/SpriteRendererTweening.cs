﻿using System;
using GameKit.Components.Drawing;

namespace GameKit.Tweening
{
    public static class SpriteRendererTweening
    {
        public static TweenGetterSetter GetTweenOpacity<T>(this T r) where T : SpriteRenderer
        {
            return new TweenGetterSetter(
                () => { return r.tintColor.A / 255f; },
                (a) => {
                    r.tintColor.A = (byte)(a * 255);
                }
            );
        }


        public static TweenGetterSetter GetTweenColorR<T>(this T r) where T : SpriteRenderer
        {
            return new TweenGetterSetter(
                () => { return r.tintColor.R / 255f; },
                (a) => {
                    r.tintColor.R = (byte)(a * 255);
                }
            );
        }

        public static TweenGetterSetter GetTweenColorG<T>(this T r) where T : SpriteRenderer
        {
            return new TweenGetterSetter(
                () => { return r.tintColor.G / 255f; },
                (a) => {
                    r.tintColor.G = (byte)(a * 255);
                }
            );
        }

        public static TweenGetterSetter GetTweenColorB<T>(this T r) where T : SpriteRenderer
        {
            return new TweenGetterSetter(
                () => { return r.tintColor.B / 255f; },
                (a) => {
                    r.tintColor.B = (byte)(a * 255);
                }
            );
        }
    }
}
