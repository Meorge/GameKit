﻿using System;
using GameKit.Components.Drawing;

namespace GameKit.Tweening
{
    public static class TextRendererTweening
    {
        public static TweenGetterSetter GetTweenFontSize(this TextRenderer r)
        {
            return new TweenGetterSetter(
                () => { return r.FontSize; },
                (a) => { r.FontSize = a; }
            );
        }

        public static TweenGetterSetter GetTweenLineSpacing(this TextRenderer r)
        {
            return new TweenGetterSetter(
                () => { return r.LineSpacing; },
                (a) => { r.LineSpacing = a; }
            );
        }

        public static TweenGetterSetter GetTweenAllCharactersOpacity(this TextRenderer r)
        {
            return new TweenGetterSetter(
                () =>
                {
                    if (r.textProperties.Length < 1) return 0f;
                    return r.textProperties[0].color.A / 255f;
                },
                (a) =>
                {
                    r.SetAllCharactersOpacity(a);
                }
            );
        }
    }
}
