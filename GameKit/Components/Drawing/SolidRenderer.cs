﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit.Drawing;

namespace GameKit.Components.Drawing
{
    public class SolidRenderer : Component
    {
        private Color color;

        public Color Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }

        public override void Draw(GameTime gT, Camera camera)
        {
            // figure out height and width
            int x = (int)gameObject.Position2D_PivotAdjusted.X;
            int y = (int)gameObject.Position2D_PivotAdjusted.Y;
            int width = (int)gameObject.ScaledDimensions.X;
            int height = (int)gameObject.ScaledDimensions.Y;

            // figure out transform matrix
            Matrix? matrix = null;
            switch (gameObject.positionSpace)
            {
                case PositionSpace.WorldSpace:
                    matrix = Engine.engine.GetActiveCamera().GetMatrix();
                    break;
                default:
                    matrix = Engine.engine.ScreenScaleMatrix;
                    break;
            }

            Rectangle destRect = new Rectangle(0, 0, width, height);

            RenderRequest request = new RenderRequest()
                .SetBatchInfo(BlendState.NonPremultiplied, null, null,
                        //Engine.engine.ScreenScaleMatrix *'
                        matrix *
                        Matrix.CreateScale(gameObject.AbsoluteScale.X, gameObject.AbsoluteScale.Y, 1f) *
                        Matrix.CreateTranslation(-gameObject.Pivot.X * gameObject.Width, -gameObject.Pivot.Y * gameObject.Height, 0f) *
                        Matrix.CreateRotationZ(gameObject.AbsoluteRotation) *
                        
                        Matrix.CreateTranslation(gameObject.AbsolutePosition.X, gameObject.AbsolutePosition.Y, 0f))
                .SetDrawSpriteInfo(
                    "_solid",
                    destRect,
                    color,
                    gameObject.Rotation,
                    Vector2.Zero,
                    SpriteEffects.None,
                    gameObject.ZRange
                );
            request.Submit();
        }
    }
}
