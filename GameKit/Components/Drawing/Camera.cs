﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Components.Drawing
{
    public class Camera : Component
    {
        public int priority = 0;

        private Matrix _transformMatrix;

        private bool keepViewportSize = false;

        public Camera()
        {
            Engine.engine.cameras.Add(this);
        }

        public Camera SetPriority(int p)
        {
            priority = p;
            return this;
        }

        public Camera KeepAtViewportSize(bool k)
        {
            keepViewportSize = k;
            return this;
        }

        public Camera SetToViewportSize()
        {
            gameObject.Width = Engine.engine.viewport.Width;
            gameObject.Height = Engine.engine.viewport.Height;
            Console.WriteLine("set camera to viewport size");
            return this;
        }

        public override void Update(GameTime gameTime)
        {
            if (keepViewportSize) SetToViewportSize();
        }

        public Matrix GetMatrix(Rectangle? rect = null)
        {
            if (rect == null) rect = Rectangle.Empty;

            Vector3 fixedPos = gameObject.Position;
            fixedPos.Z = 0;

            Vector3 origin = new Vector3(Engine.engine.viewport.Width * 0.5f, Engine.engine.viewport.Height * 0.5f, 0);

            _transformMatrix =
                //Matrix.CreateTranslation(-(float)rect?.X, -(float)rect?.Y, 0f) *
                //Engine.engine.ScreenScaleMatrix *
                //Matrix.CreateTranslation((float)rect?.X, (float)rect?.Y, 0f) *
                Matrix.CreateTranslation(origin) *
                
                Matrix.CreateTranslation(fixedPos) *
                Matrix.CreateRotationZ(gameObject.Rotation) *
                Matrix.CreateScale(new Vector3(gameObject.scale, 1));
                

            return _transformMatrix;
        }

        public Vector2 ToWorldSpace(Vector2 camSpaceVec)
        {
            return Vector2.Transform(camSpaceVec, Matrix.Invert(GetMatrix()));
        }

        public Vector2 ToScreenSpace(Vector2 worldSpaceVec)
        {
            return Vector2.Transform(worldSpaceVec, GetMatrix());
        }
    }
}
