﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit.Assets;
using GameKit.Drawing;


namespace GameKit.Components.Drawing
{
    public class NineSliceSpriteRenderer : SpriteRenderer
    {
        public override void Draw(GameTime gT, Camera camera)
        {

            NineSliceSprite sprite = (NineSliceSprite)Engine.engine.assets[spriteID];

            Texture2D spriteTex = Engine.engine.textures[sprite.sourceTexture];

            // figure out height and width
            int height, width;

            switch (sclType)
            {
                case SpriteScaleType.ImageSize:
                    width = spriteTex.Width * (int)gameObject.scale.X;
                    height = spriteTex.Height * (int)gameObject.scale.Y;
                    break;
                default:
                    width = (int)gameObject.ScaledDimensions.X;
                    height = (int)gameObject.ScaledDimensions.Y;
                    break;
            }

            // figure out transform matrix
            Matrix? matrix = null;
            switch (gameObject.positionSpace)
            {
                case PositionSpace.WorldSpace:
                    matrix = Engine.engine.GetActiveCamera().GetMatrix();
                    break;
                default:
                    matrix = Matrix.Identity;
                    break;
            }

            Rectangle[] sourceSlices = GKMath.CreateNineSlice(
                Engine.engine.textures[sprite.sourceTexture].Bounds,
                sprite.leftPadding,
                sprite.rightPadding,
                sprite.topPadding,
                sprite.bottomPadding
                );

            Rectangle[] destinationSlices = GKMath.CreateNineSlice(
                new Rectangle(0, 0, gameObject.rectangle.Width, gameObject.rectangle.Height),
                sprite.leftPadding,
                sprite.rightPadding,
                sprite.topPadding,
                sprite.bottomPadding
                );

            for (int i = 0; i < sourceSlices.Length; i++)
            {
                if (destinationSlices[i].Height == 0 || destinationSlices[i].Width == 0) continue;
                
                RenderRequest request = new RenderRequest()
                    .SetBatchInfo(BlendState.NonPremultiplied, null, null,
                        //Matrix.CreateTranslation(x, y, 0f) *
                        Matrix.CreateScale(new Vector3(gameObject.AbsoluteScale.X, gameObject.AbsoluteScale.Y, 1f)) *
                        Matrix.CreateTranslation(-gameObject.Pivot.X * gameObject.Width, -gameObject.Pivot.Y * gameObject.Height, 0f) *
                        Matrix.CreateRotationZ(gameObject.Rotation) *
                        matrix *
                        //Matrix.CreateTranslation(-x, -y, 0f)
                        Matrix.CreateTranslation(gameObject.AbsolutePosition.X, gameObject.AbsolutePosition.Y, 0f)
                        
                    )
                    .SetDrawSpriteInfo(
                        spriteID,
                        destinationSlices[i],
                        tintColor,
                        0f,
                        Vector2.Zero,
                        SpriteEffects.None,
                        gameObject.ZRange,
                        sourceSlices[i]
                    );
                request.Submit();
            }


        }
    }
}
