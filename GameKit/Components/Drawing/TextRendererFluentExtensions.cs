﻿using System;
using Microsoft.Xna.Framework;
namespace GameKit.Components.Drawing
{
    public static class TextRendererFluentExtensions
    {

        public static T SetFontName<T>(this T o, string f) where T : TextRenderer
        {
            o.FontName = f;
            return o;
        }

        public static T SetText<T>(this T o, string t) where T : TextRenderer
        {
            o.Text = t;
            return o;
        }

        public static T SetDefaultColor<T>(this T o, Color c) where T : TextRenderer
        {
            o.DefaultColor = c;
            return o;
        }

        public static T SetTintColor<T>(this T o, Color c) where T : TextRenderer
        {
            o.tintColor = c;
            return o;
        }

        public static T SetUseRichText<T>(this T o, bool b) where T : TextRenderer
        {
            o.UseRichText = b;
            return o;
        }

        public static T SetTextRenderMode<T>(this T o, TextRenderer.TextRenderMode m) where T : TextRenderer
        {
            o.RenderMode = m;
            return o;
        }

        public static T SetFitRectangleToText<T>(this T o, bool v) where T : TextRenderer
        {
            o.fitGameObjectToText = v;
            return o;
        }


        public static T SetHorizontalAlignment<T>(this T o, TextRenderer.HorizontalAlignment h) where T : TextRenderer
        {
            o.horizontalAlignment = h;
            return o;
        }

        public static T SetVerticalAlignment<T>(this T o, TextRenderer.VerticalAlignment v) where T : TextRenderer
        {
            o.verticalAlignment = v;
            return o;
        }

        public static T SetLineSpacing<T>(this T o, float l) where T : TextRenderer
        {
            o.LineSpacing = l;
            return o;
        }

        public static T SetFontSize<T>(this T o, float l) where T : TextRenderer
        {
            o.FontSize = l;
            return o;
        }
    }
}
