﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameKit.Components.Drawing
{
    public static class SpriteRendererExtensions
    {
        public static T SetSprite<T>(this T t, string tex) where T : SpriteRenderer
        {
            t.spriteID = tex;
            return t;
        }

        public static T SetTintColor<T>(this T t, Color col) where T : SpriteRenderer
        {
            t.tintColor = col;
            return t;
        }

        public static T SetTintColorRGB<T>(this T t, Color col) where T : SpriteRenderer
        {
            t.tintColor.R = col.R;
            t.tintColor.G = col.G;
            t.tintColor.B = col.B;
            return t;
        }

        public static T SetScaleType<T>(this T t, SpriteScaleType s) where T : SpriteRenderer
        {
            t.sclType = s;
            return t;
        }

        public static T SetFlipEffect<T>(this T t, SpriteEffects s) where T : SpriteRenderer
        {
            t.flipEffects = s;
            return t;
        }
    }
}
