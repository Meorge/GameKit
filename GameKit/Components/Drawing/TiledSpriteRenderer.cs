﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit.Assets;
using GameKit.Drawing;

namespace GameKit.Components.Drawing
{
    public class TiledSpriteRenderer : SpriteRenderer
    {

        public int xAmount = 50;
        public int yAmount = 50;

        public override void Draw(GameTime gT, Camera camera)
        {
            Sprite sprite = (Sprite)Engine.engine.assets[spriteID];
            Texture2D spriteTex = Engine.engine.textures[sprite.sourceTexture];

            Rectangle rect = GetRectangle(spriteTex);

            for (int x = rect.X - (rect.Width * xAmount); x <= rect.X + (rect.Width * xAmount); x += rect.Width)
            {
                for (int y = rect.Y - (rect.Height * yAmount); y <= rect.Y + (rect.Height * yAmount); y += rect.Height)
                {
                    Rectangle r = rect;
                    r.X = x;
                    r.Y = y;

                    RenderRequest request = new RenderRequest()
                        .SetBatchInfo(BlendState.NonPremultiplied, null, null, GetMatrix())
                        .SetDrawSpriteInfo(
                            spriteID,
                            r,
                            tintColor,
                            0f,
                            Vector2.Zero,
                            SpriteEffects.None,
                            gameObject.ZRange
                        );
                    request.Submit();
                }
            }
        }
    }
}
