﻿using System;
using Microsoft.Xna.Framework;
using GameKit.Assets.Animation;

namespace GameKit.Components.Drawing
{
    public class SpriteAnimator : Component
    {
        public string animationName { get; private set; } = "";

        private SpriteRenderer renderer;
        private SpriteAnimation currentAnimation = null;
        private SpriteAnimationFrame currentFrame = null;
        private float durationOfThisFrame = 0f;
        private int spriteFrameIndex = 0;

        public bool Play { get; set; } = true;

        public override void Create()
        {
            renderer = gameObject.GetComponent<SpriteRenderer>();
        }

        public void LoadAnimation(string name)
        {
            animationName = name;

            if (name == "" || !Engine.engine.assets.ContainsKey(name))
            {
                renderer.SetSprite("");
                Play = false;
                return;
            }

            Play = true;
            currentAnimation = (SpriteAnimation)Engine.engine.assets[name];
            spriteFrameIndex = currentAnimation.loopStart;
            currentFrame = currentAnimation.frames[spriteFrameIndex];
            durationOfThisFrame = 0f;

            if (renderer != null)
            {
                renderer.SetSprite(currentAnimation.prefix + currentFrame.name);
            }

            //Console.WriteLine("Loaded animation!");
            //Console.WriteLine($"Prefix of this animation is {currentAnimation.prefix}");

            foreach (SpriteAnimationFrame frame in currentAnimation.frames)
            {
                //Console.WriteLine($"Frame name = {currentAnimation.prefix}{frame.name}, show for {frame.duration} frames");
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (currentAnimation == null) return;
            if (!Play) return;

            durationOfThisFrame += (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Console.WriteLine($"Duration of this frame is {durationOfThisFrame}, current frame index is {spriteFrameIndex}");
            if (currentFrame.duration <= durationOfThisFrame)
            {
                // go to next frame
                spriteFrameIndex++;
                durationOfThisFrame = 0f;

                // if we're over the limit, reset to zero
                if (currentAnimation.loopEnd <= spriteFrameIndex) spriteFrameIndex = currentAnimation.loopStart;

                currentFrame = currentAnimation.frames[spriteFrameIndex];

                //Console.WriteLine($"Go to the next frame, frame {spriteFrameIndex}");

                if (renderer != null)
                {
                    renderer.SetSprite(currentAnimation.prefix + currentFrame.name);
                }
            }
        }
    }
}
