﻿using System;
using Microsoft.Xna.Framework;
namespace GameKit.Components.Drawing
{
    public static class SolidRendererFluentExtensions
    {
        public static T SetColor<T>(this T o, Color c) where T : SolidRenderer
        {
            o.Color = c;
            return o;
        }
    }
}
