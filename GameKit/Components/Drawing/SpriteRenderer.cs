﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit.Assets;
using GameKit.Drawing;


namespace GameKit.Components.Drawing
{
    public class SpriteRenderer : Component
    {
        public string spriteID;

        public Color tintColor = Color.White;

        public SpriteScaleType sclType = SpriteScaleType.ScaleToFitRectangle;

        public SpriteEffects flipEffects = SpriteEffects.None;

        protected Matrix GetMatrix()
        {
            Matrix matrix = Matrix.Identity;
            if (gameObject.positionSpace == PositionSpace.WorldSpace)
                matrix = Engine.engine.GetActiveCamera().GetMatrix(gameObject.rectangle);

            return matrix *
                        //Matrix.CreateScale(gameObject.AbsoluteScale.X, gameObject.AbsoluteScale.Y, 1f) *
                        Matrix.CreateTranslation(-gameObject.Pivot.X * gameObject.Width * gameObject.AbsoluteScale.X, -gameObject.Pivot.Y * gameObject.Height * gameObject.AbsoluteScale.Y, 0f) *
                        Matrix.CreateRotationZ(gameObject.AbsoluteRotation) *
                        Matrix.CreateTranslation(gameObject.AbsolutePosition.X, gameObject.AbsolutePosition.Y, 0f);
        }

        protected Rectangle GetRectangle(Texture2D spriteTex)
        {
            int height = gameObject.ScaledDimensions.Y.RoundToInt();
            int width = gameObject.ScaledDimensions.X.RoundToInt();

            int x = 0;
            int y = 0;

            if (sclType == SpriteScaleType.ImageSize)
            {
                width = (spriteTex.Width * gameObject.AbsoluteScale.X).RoundToInt();
                height = (spriteTex.Height * gameObject.AbsoluteScale.Y).RoundToInt();
            }

            else if (sclType == SpriteScaleType.ScaleToFitRectangle_MaintainRatio)
            {
                if (height > width)
                {
                    // height is greater than width
                    // so, the height should be the same as the gameobjects scale
                    // and the width should be adjusted accordingly

                    height = gameObject.ScaledDimensions.Y.RoundToInt();

                    float ratio = (float)spriteTex.Height / gameObject.ScaledDimensions.Y;

                    width = (spriteTex.Width / ratio).RoundToInt();

                    x = (gameObject.ScaledDimensions.X / ratio - width / 2).RoundToInt() / 2;
                } else
                {
                    // width is greater than height
                    // so, the width should be the same as the gameobjects scale
                    // and the height should be adjusted accordingly

                    width = gameObject.ScaledDimensions.X.RoundToInt();

                    float ratio = (float)spriteTex.Width / gameObject.ScaledDimensions.X;

                    height = (spriteTex.Height / ratio).RoundToInt();
                }
            }
            return new Rectangle(x, y, width, height);
        }

        public override void Draw(GameTime gT, Camera camera)
        {
            if (spriteID == null || spriteID == "") return;// spriteID = "_solid";
            Sprite sprite = (Sprite)Engine.engine.assets[spriteID];
            Texture2D spriteTex = Engine.engine.textures[sprite.sourceTexture];

            RenderRequest request = new RenderRequest()
                .SetBatchInfo(BlendState.NonPremultiplied, null, null, GetMatrix())
                .SetDrawSpriteInfo(
                    spriteID,
                    GetRectangle(spriteTex),
                    tintColor,
                    0f,
                    Vector2.Zero,
                    flipEffects,
                    gameObject.ZRange
                );
            request.Submit();
        }
    }

    public enum SpriteScaleType
    {
        ImageSize = 0,
        ScaleToFitRectangle = 1,
        ScaleToFitRectangle_MaintainRatio = 2
    }
}
