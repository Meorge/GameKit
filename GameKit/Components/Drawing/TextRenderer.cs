﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameKit;
using GameKit.Drawing;
using GameKit.Utilities;
using GameKit.Assets;

namespace GameKit.Components.Drawing
{
    public class TextRenderer : Component
    {
        private string fontName;
        
        public string text = "";
        private string displayText = "";

        public Color defaultColor = Color.White;
        public Color tintColor;

        public TextRenderMode renderMode = TextRenderMode.PerCharacter;

        public TextProperty[] textProperties;
        

        public bool fitGameObjectToText = false;

        private float _lineSpacing = 0f;
        private float _fontSize = 1f;


        public HorizontalAlignment horizontalAlignment = HorizontalAlignment.AlignLeft;
        public VerticalAlignment verticalAlignment = VerticalAlignment.AlignTop;


        // rich text
        private bool useRichText = false;
        private TagParser parser = new TagParser();
        private TextProperty[] richTextProperties;

        public bool UseRichText
        {
            get
            {
                return useRichText;
            }

            set
            {
                useRichText = value;
                Text = text;
            }
        }


        public string FontName
        {
            get
            {
                return FontName;
            }

            set
            {
                fontName = value;
            }
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {

                text = value.Replace("\\n", System.Environment.NewLine);

                if (useRichText)
                {
                    parser.ParseText(text);

                    displayText = parser.HumanReadableText;
                } else
                {
                    displayText = text;
                }
                UpdateTextPropertyArray();
            }
        }

        public string DisplayText
        {
            get
            {
                return displayText;
            }
        }

        public float LineSpacing
        {
            get
            {
                return _lineSpacing;
            }

            set
            {
                _lineSpacing = value;
            }
        }

        public float FontSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                _fontSize = value;
            }
        }

        public TextRenderMode RenderMode
        {
            get
            {
                return renderMode;
            }

            set
            {
                renderMode = value;
                UpdateTextPropertyArray();
            }
        }

        public Color DefaultColor
        {
            get
            {
                return defaultColor;
            }

            set
            {
                defaultColor = value;
            }
        }

        public enum HorizontalAlignment
        {
            AlignLeft,
            AlignCenter,
            AlignRight
        }

        public enum VerticalAlignment
        {
            AlignTop,
            AlignCenter,
            AlignBottom
        }
        

        public enum TextRenderMode
        {
            PerCharacter = 0,
            PerString = 1
        }


        public List<TagObject> GetTagsAtPosition(int pos)
        {
            if (!useRichText) return null;

            return parser.tags.FindAll(a => a.startIndex == pos && a.endIndex == pos);
        }

        public void SetAllCharactersOpacity(float opacity)
        {
            for (int i = 0; i < textProperties.Length; i++)
            {
                textProperties[i].color.A = (byte)(opacity * 255);
            }
        }


        public virtual void UpdateTextPropertyArray()
        {
            TextProperty defaultTextProperty = new TextProperty(Vector2.Zero, DefaultColor);
            if (renderMode == TextRenderMode.PerCharacter)
            {
                textProperties = new TextProperty[displayText.Length];
                textProperties.Fill(defaultTextProperty);

                richTextProperties = new TextProperty[displayText.Length];
                richTextProperties.Fill(defaultTextProperty);

                if (useRichText)
                {
                    //Console.WriteLine("rich text time boi");
                    //Console.WriteLine($"text is {text}");
                    // let's check for color tags
                    foreach (TagObject tag in parser.tags.FindAll(a => a.Name == "color"))
                    {
                        //Console.WriteLine(tag);
                        //Console.WriteLine(displayText.Substring(tag.startIndex, tag.endIndex - tag.startIndex));

                        string prop = tag.Arguments[0];
                        //Console.WriteLine(prop.Length);
                        Color c = Color.White;

                        switch (prop)
                        {
                            case "red":
                                //Console.WriteLine($"Color the text \"{displayText.Substring(tag.startIndex, tag.endIndex - tag.startIndex)}\" red");
                                c = Color.Red;
                                break;
                            case "green":
                                //Console.WriteLine($"Color the text \"{displayText.Substring(tag.startIndex, tag.endIndex - tag.startIndex)}\" green");
                                c = Color.Green;
                                break;
                            case "blue":
                                //Console.WriteLine($"Color the text \"{displayText.Substring(tag.startIndex, tag.endIndex - tag.startIndex)}\" blue");
                                c = Color.Blue;
                                break;
                            case "orange":
                                c = Color.OrangeRed;
                                break;
                            default:
                                Console.WriteLine($"The key \"{prop}\" didn't match anything");
                                break;
                        }

                        for (int i = tag.startIndex; i < tag.endIndex; i++)
                        {
                            richTextProperties[i].color = c;
                        }
                    }
                }
            }
            else
            {
                textProperties = new TextProperty[1];
                textProperties[0] = defaultTextProperty;

                richTextProperties = new TextProperty[displayText.Length];
                richTextProperties.Fill(defaultTextProperty);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (fitGameObjectToText)
            {
                SpriteFont s = ((Font)Engine.engine.assets[fontName]).spriteFont;
                Vector2 m = s.MeasureString(displayText);
                gameObject.SetDimensions((int)(m.X * gameObject.scale.X), (int)(m.Y * gameObject.scale.Y));

                //Console.WriteLine($"Making {gameObject.name} dimensions be {m}, they are {gameObject.Dimensions}");
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, Camera camera)
        {
            // figure out transform matrix
            Matrix camMtx = Matrix.Identity;
            switch (gameObject.positionSpace)
            {
                case PositionSpace.WorldSpace:
                    camMtx = Engine.engine.GetActiveCamera().GetMatrix();
                    break;
                default:
                    break;
            }

            RenderRequest request;

            Vector2 sizeOfString = ((Font)Engine.engine.assets[fontName]).spriteFont.MeasureString(displayText);

            switch (renderMode)
            {
                case TextRenderMode.PerCharacter:
                    Vector2 totalOffset = Vector2.Zero;

                    switch (horizontalAlignment)
                    {
                        case HorizontalAlignment.AlignLeft:
                            totalOffset.X = 0f;
                            break;
                        case HorizontalAlignment.AlignRight:
                            totalOffset.X += (gameObject.Width) - sizeOfString.X * gameObject.scale.X * FontSize;
                            break;
                        case HorizontalAlignment.AlignCenter:
                            totalOffset.X += ((gameObject.Width) / 2) - (sizeOfString.X * gameObject.scale.X / 2) * FontSize;
                            break;
                    }

                    switch (verticalAlignment)
                    {
                        case VerticalAlignment.AlignTop:
                            totalOffset.Y = 0f;
                            break;
                        case VerticalAlignment.AlignBottom:
                            totalOffset.Y += (gameObject.Height) - sizeOfString.Y * gameObject.scale.Y * FontSize;
                            break;
                        case VerticalAlignment.AlignCenter:
                            totalOffset.Y += ((gameObject.Height) / 2) - (sizeOfString.Y * gameObject.scale.Y / 2) * FontSize;
                            break;
                    }

                    totalOffset /= FontSize;

                    for (int i = 0; i < displayText.Length; i++)
                    {
                        char c = displayText[i];
                        Vector2 sizeOfChar = ((Font)Engine.engine.assets[fontName]).spriteFont.MeasureString(c.ToString()) * gameObject.scale;
                        sizeOfChar.Y = 0;

                        if (c == '\n')
                        {
                            totalOffset.Y += (((Font)Engine.engine.assets[fontName]).spriteFont.LineSpacing + LineSpacing) * gameObject.scale.Y;
                            totalOffset.X = 0;
                        }

                        Color finalColor = tintColor.Multiply(textProperties[i].color).Multiply(richTextProperties[i].color);

                        request = new RenderRequest()
                            .SetBatchInfo(BlendState.NonPremultiplied, SamplerState.AnisotropicClamp, null,
                                camMtx *
                                Matrix.CreateScale(new Vector3(gameObject.AbsoluteScale.X, gameObject.AbsoluteScale.Y, 1f)) *
                                Matrix.CreateTranslation(-gameObject.Pivot.X * gameObject.Width, -gameObject.Pivot.Y * gameObject.Height, 0f) *
                                Matrix.CreateRotationZ(gameObject.AbsoluteRotation) *
                                Matrix.CreateTranslation(gameObject.AbsolutePosition.X, gameObject.AbsolutePosition.Y, 0f)
                                )
                            .SetDrawTextInfo(
                                fontName,
                                c.ToString(),
                                (totalOffset * FontSize + textProperties[i].offset),
                                finalColor,
                                0f,
                                Vector2.Zero,
                                gameObject.AbsoluteScale.ToVector2() * FontSize,
                                SpriteEffects.None,
                                gameObject.ZRange
                            );
                        request.Submit();
                        

                        totalOffset += sizeOfChar;
                    }
                    break;
                case TextRenderMode.PerString:
                    request = new RenderRequest()
                        .SetBatchInfo(BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, camMtx)
                        .SetDrawTextInfo(
                            fontName,
                            displayText,
                            gameObject.Position2D + textProperties[0].offset,
                            tintColor.Multiply(textProperties[0].color),
                            0f,
                            Vector2.Zero,
                            Vector2.One,
                            SpriteEffects.None,
                            gameObject.ZRange
                        );
                    request.Submit();
                    break;
            }
        }
    }
}
