﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameKit.Components.Drawing;

namespace GameKit.Components.Collision
{
    public class RectCollider : Collider
    {
        public new RectCollider AddListenerOnCollisionEnter(CollisionAction d) { return (RectCollider)(base.AddListenerOnCollisionEnter(d)); }
        public new RectCollider AddListenerOnCollisionStay(CollisionAction d) { return (RectCollider)(base.AddListenerOnCollisionStay(d)); }
        public new RectCollider AddListenerOnCollisionLeave(CollisionAction d) { return (RectCollider)(base.AddListenerOnCollisionLeave(d)); }

        public override void Draw(GameTime gameTime, Camera camera)
        {
            //Texture2D t = new Texture2D(Engine.engine.graphicsDeviceManager.GraphicsDevice, 1, 1);
            //Color[] data = new Color[1];
            //data[0] = new Color(0.9f, 0.0f, 0.0f, 0.1f);
            //t.SetData(data);

            //Engine.engine.spriteBatch.Begin(transformMatrix: Engine.engine.GetActiveCamera().GetMatrix());
            //Engine.engine.spriteBatch.Draw(
            //    t,
            //    new Rectangle(gameObject.X, gameObject.Y, gameObject.Width, gameObject.Height),
            //    new Rectangle(0,0,1,1),
            //    Color.White,
            //    0f,
            //    new Vector2((gameObject.scale.X) / 2f, (gameObject.scale.Y) / 2f),
            //    SpriteEffects.None,
            //    gameObject.Z
            //    );
            //Engine.engine.spriteBatch.End();
            base.Draw(gameTime, camera);
        }

        public override bool Colliding(Collider other)
        {
            //Console.WriteLine("rectcollider thing");
            if (other is RectCollider)
            {
                return gameObject.rectangle.Intersects(other.gameObject.rectangle);
            }

            return false;
        }


    }
}
