﻿using System;
using System.Collections.Generic;

namespace GameKit.Components.Collision
{
    public class Collider : Component
    {
        private static List<Collider> colliders = new List<Collider>();

        private static List<Collision> previousCollisions = new List<Collision>();
        private static List<Collision> currentCollisions = new List<Collision>();

        public static void CheckForCollisions()
        {
            previousCollisions = new List<Collision>(currentCollisions);
            currentCollisions.Clear();

            //Console.WriteLine("--------- COLLISION START ---------");
            foreach (Collider c in colliders)
            {
                foreach (Collider d in colliders)
                {
                    //Console.WriteLine($"COLL - {c.gameObject.name} x {d.gameObject.name}");
                    if (c.GetHashCode() == d.GetHashCode()) continue;

                    Collision _cA = new Collision(c, d);
                    Collision _cB = new Collision(d, c);

                    if (currentCollisions.Contains(_cA) || currentCollisions.Contains(_cB))
                    {
                        continue;
                    }

                    if (c.Colliding(d))
                    {
                        currentCollisions.Add(_cA);
                    }
                }
            }

            foreach (Collision coll in currentCollisions)
            {
                if (!previousCollisions.Contains(coll))
                {
                    //Console.WriteLine("Collision begun!");
                    if (coll.A.onCollisionEnter != null) coll.A.onCollisionEnter.Invoke(coll.B);
                    if (coll.B.onCollisionEnter != null) coll.B.onCollisionEnter.Invoke(coll.A);
                } else {
                    //Console.WriteLine("Collision stay!");
                    if (coll.A.onCollisionStay != null) coll.A.onCollisionStay.Invoke(coll.B);
                    if (coll.B.onCollisionStay != null) coll.B.onCollisionStay.Invoke(coll.A);
                }
            }

            foreach (Collision coll in previousCollisions)
            {
                if (!currentCollisions.Contains(coll))
                {
                    //Console.WriteLine("Collision ended!");
                    if (coll.A.onCollisionLeave != null) coll.A.onCollisionLeave.Invoke(coll.B);
                    if (coll.B.onCollisionLeave != null) coll.B.onCollisionLeave.Invoke(coll.A);
                }
            }

            //Console.WriteLine("--------- COLLISION DONE ---------");
        }

        public Collider() : base()
        {
            //Console.WriteLine($"Creating collider {gameObject.name}");
            colliders.Add(this);
        }

        public override void Dispose()
        {
            //Console.WriteLine($"Dispose of collider {gameObject.name}");
            colliders.Remove(this);
        }


        public delegate void CollisionAction(Collider other);

        public event CollisionAction onCollisionEnter;
        public event CollisionAction onCollisionStay;
        public event CollisionAction onCollisionLeave;


        public Collider AddListenerOnCollisionEnter(CollisionAction d)
        {
            onCollisionEnter += d;
            return this;
        }

        public Collider AddListenerOnCollisionStay(CollisionAction d)
        {
            onCollisionStay += d;
            return this;
        }

        public Collider AddListenerOnCollisionLeave(CollisionAction d)
        {
            onCollisionLeave += d;
            return this;
        }

        public virtual bool Colliding(Collider other) { return false; }
    }

    public struct Collision
    {
        public Collider A;
        public Collider B;

        public Collision(Collider a, Collider b)
        {
            A = a;
            B = b;
        }
    }
}
