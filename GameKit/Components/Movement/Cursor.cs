﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using GameKit.Components.Drawing;

namespace GameKit.Components.Movement
{
    public class Cursor : Component
    {

        public Vector2 alignment = Vector2.Zero;

        public Cursor SetAlignment(Vector2 align)
        {
            alignment = align;
            return this;
        }

        public override void Update(GameTime gameTime)
        {
            MouseState state = Mouse.GetState();
            Camera activeCamera = Engine.engine.GetActiveCamera();

            Matrix proj = Matrix.CreateOrthographic(Engine.engine.BaseWidth, Engine.engine.BaseHeight, 0f, 1000f);
            Matrix world = Matrix.CreateWorld(activeCamera.gameObject.Position, new Vector3(0, 0, 1), new Vector3(0, -1, 0));

            Vector3 pos = Engine.engine.viewport.Unproject(new Vector3(state.X, state.Y, 0), proj, activeCamera.GetMatrix(), world);

            Vector2 pos2D = new Vector2(pos.X, pos.Y);

            gameObject.Position2D = pos2D + new Vector2(Engine.engine.BaseWidth / 2, -Engine.engine.BaseHeight / 2)
                - new Vector2(gameObject.Width * alignment.X, gameObject.Height * alignment.Y)
                + new Vector2(activeCamera.gameObject.X, activeCamera.gameObject.Y);
        }


    }
}
