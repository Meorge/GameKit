﻿using System;
namespace GameKit.Components.UI
{
    public class UIAbstractAligner : Component
    {
        private int padding = 0;

        public int Padding
        {
            get { return padding; }
            set { padding = value; }
        }
    }
}
