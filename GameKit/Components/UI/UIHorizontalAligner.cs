﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

using GameKit.InputManagement;

namespace GameKit.Components.UI
{
    public class UIHorizontalAligner : UIAbstractAligner
    {
        public bool extraStuff = false;

        public override void Update(GameTime gameTime)
        {
            List<GameObject> children = gameObject.Children.FindAll((a) => a.GetComponent<UIIgnoreAligner>() == null);
            // Get the width of the children all together
            int widthSum = 0;
            Rectangle sumRect = Rectangle.Empty;
            foreach (GameObject child in children)
            {
                
                child.RawX = (widthSum * 2) - gameObject.AbsoluteRectangle.Width;
                widthSum += child.AbsoluteRectangle.Width / 2;
                widthSum += Padding;

                sumRect = sumRect == Rectangle.Empty ? child.AbsoluteRectangle : Rectangle.Union(sumRect, child.AbsoluteRectangle);
            }

            // Find distance from children sum center to parent center
            int xPosOfSumRectCenter = (int)(sumRect.Width / 2);
            int xPosOfParentCenter = gameObject.AbsoluteRectangle.Width / 2;

            int distance = xPosOfParentCenter - xPosOfSumRectCenter;

            // Add this distance to all of the childrens' Xs
            foreach (GameObject child in children)
            {
                //Console.WriteLine($"Child with name {child.name} has X {child.X}");
                child.X += distance;

                // if the physics sim's ui is breaking cuz of horizontal align stuff,
                // try commenting this line. it makes stuff work in atticus
                if (extraStuff) child.X += gameObject.Width / 2;
            }
        }
    }
}
