﻿using System;
using GameKit;
using Microsoft.Xna.Framework;

namespace GameKit.Components.UI
{
    public class UITransform : Component
    {
        private Rectangle currentRect;

        private Vector2 anchorMin = Vector2.Zero;
        private Vector2 anchorMax = Vector2.Zero;

        private UITransformInfo position = UITransformInfo.Zero;

        public UITransformInfo Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                UpdateRectangle2ElectricBoogaloo();
            }
        }

        public Vector2 AnchorMin
        {
            get
            {
                return anchorMin;
            }
            set
            {
                anchorMin = value;
                UpdateRectangle2ElectricBoogaloo();
            }
        }

        public Vector2 AnchorMax
        {
            get
            {
                return anchorMax;
            }

            set
            {
                anchorMax = value;
                UpdateRectangle2ElectricBoogaloo();
            }
        }


        public UITransform SetAnchorMin(Vector2 a)
        {
            AnchorMin = a;

            return this;
        }

        public UITransform SetAnchorMin(float x, float y)
        {
            AnchorMin = new Vector2(x, y);
            return this;
        }

        public UITransform SetAnchorMax(Vector2 a)
        {
            AnchorMax = a;
            return this;
        }

        public UITransform SetAnchorMax(float x, float y)
        {
            AnchorMax = new Vector2(x, y);
            return this;
        }

        public UITransform SetValues(UITransformInfo v)
        {
            position = v;
            return this;
        }

        public UITransform SetValues(int left, int top, int right, int bottom)
        {
            Position = new UITransformInfo(left, right, top, bottom);
            return this;
        }

        public override void Update(GameTime gameTime)
        {
            UpdateRectangle2ElectricBoogaloo();
        }

        public void UpdateRectangle2ElectricBoogaloo()
        {
            int parentWidth, parentHeight;
            if (gameObject.Parent != null)
            {
                parentWidth = (int)(gameObject.Parent.rectangle.Width);
                parentHeight = (int)(gameObject.Parent.rectangle.Height);
            }
            else
            {
                parentWidth = (int)(Engine.engine.TargetWidth);
                parentHeight = (int)(Engine.engine.TargetHeight);
            }


            float x = parentWidth * AnchorMin.X + Position.Left;
            float y = parentHeight * AnchorMin.Y + (anchorMin.Y != anchorMax.Y ? Position.Bottom : Position.Top);

            //y *= Engine.engine.HeightRatio;

            float width, height;
            if (AnchorMin.X == AnchorMax.X)
            {
                width = Position.Right;
            } else
            {
                width = (parentWidth * AnchorMax.X - Position.Right) - (parentWidth * AnchorMin.X + Position.Left);
            }

            if (AnchorMin.Y == AnchorMax.Y)
            {
                height = Position.Bottom;
            } else
            {
                height = (parentHeight * AnchorMax.Y - Position.Bottom) - (parentHeight * AnchorMin.Y + Position.Top);
            }

            //y -= gameObject.Pivot.Y * gameObject.Width;
            

            gameObject.Width = (int)width;
            gameObject.Height = (int)height;


            //x -= gameObject.Parent != null ? gameObject.Parent.X : 0;
            //y -= gameObject.Parent != null ? gameObject.Parent.Y : 0;
            //x -= gameObject.Parent != null ? gameObject.Width * gameObject.Pivot.X : 0;
            //y -= gameObject.Parent != null ? gameObject.Height * gameObject.Pivot.Y : 0;

            if (gameObject.Parent != null)
            {
                float tempX = x;
                //x -= x;
                x -= gameObject.Parent.Width * gameObject.Parent.Pivot.X;
                y -= gameObject.Parent.Height * gameObject.Parent.Pivot.Y;
                //x -= tempX;

                x += gameObject.Width * gameObject.Pivot.X;
                y += gameObject.Height * gameObject.Pivot.Y;
            }




            gameObject.X = x.RoundToInt();// - (gameObject.Pivot.X * gameObject.Width).RoundToInt();
            gameObject.Y = y.RoundToInt();// - (gameObject.Pivot.Y * gameObject.Height).RoundToInt();

            //Console.WriteLine($"Name:{gameObject.name}\nX:{x}\nY:{y}\nWidth:{width}\nHeight:{height}\n\n");
        }
    }


    public struct UITransformInfo
    {
        public int Left;
        public int Right;
        public int Top;
        public int Bottom;


        public static UITransformInfo Zero
        {
            get
            {
                return new UITransformInfo(0, 0, 0, 0);
            }
        }

        public UITransformInfo(int l, int r, int t, int b)
        {
            Left = l;
            Right = r;
            Top = t;
            Bottom = b;
        }

        public override string ToString()
        {
            return $"{{Left: {Left} Right: {Right} Top: {Top} Bottom: {Bottom}}}";
        }
    }
}
