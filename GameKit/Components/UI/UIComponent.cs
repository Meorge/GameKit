﻿using System;
using Microsoft.Xna.Framework;

using GameKit.InputManagement;
namespace GameKit.Components.UI
{
    public class UIComponent : Component
    {
        bool previouslyContainedMouse = false;
        bool nowContainsMouse = false;

        bool leftClickDragInProgress = false;
        bool middleClickDragInProgress = false;
        bool rightClickDragInProgress = false;

        public bool ContainsMouse { get { return nowContainsMouse; } }

        public delegate void MouseEvent();
        public delegate void MouseDragEvent(Input.MouseButton button, Vector2 movement);
        public delegate void MouseScrollWheelEvent(int scrollAmount);

        public event MouseEvent OnMouseEnter;
        public event MouseEvent OnMouseStay;
        public event MouseEvent OnMouseLeave;

        public event MouseEvent OnMouseLeftClick;
        public event MouseEvent OnMouseMiddleClick;
        public event MouseEvent OnMouseRightClick;

        public event MouseEvent OnMouseLeftPressIn;
        public event MouseEvent OnMouseMiddlePressIn;
        public event MouseEvent OnMouseRightPressIn;

        public event MouseEvent OnMouseLeftRelease;
        public event MouseEvent OnMouseMiddleRelease;
        public event MouseEvent OnMouseRightRelease;

        public event MouseEvent OnMouseLeftPress;
        public event MouseEvent OnMouseMiddlePress;
        public event MouseEvent OnMouseRightPress;

        public event MouseDragEvent OnMouseDrag;

        public event MouseScrollWheelEvent OnMouseHorizontalScroll;
        public event MouseScrollWheelEvent OnMouseVerticalScroll;

        public override void Update(GameTime gameTime)
        {
            Vector2 mousePos = Input.CursorPosition(gameObject.positionSpace);


            Rectangle r = gameObject.AbsoluteRectangle;
            nowContainsMouse = r.Contains(mousePos);

            if (!previouslyContainedMouse && nowContainsMouse) onMouseEnter();
            if (previouslyContainedMouse && nowContainsMouse) onMouseStay();
            if (previouslyContainedMouse && !nowContainsMouse) onMouseLeave();


            // for dragging
            //if (Input.MouseButtonReleased(Input.MouseButton.LeftButton)) leftClickDragInProgress = false;
            //if (Input.MouseButtonReleased(Input.MouseButton.MiddleButton)) middleClickDragInProgress = false;
            //if (Input.MouseButtonReleased(Input.MouseButton.RightButton)) rightClickDragInProgress = false;
            //if (leftClickDragInProgress && Input.MouseButtonPressed(Input.MouseButton.LeftButton)) onMouseLeftPress();
            //if (middleClickDragInProgress && Input.MouseButtonPressed(Input.MouseButton.MiddleButton)) onMouseMiddlePress();
            //if (rightClickDragInProgress && Input.MouseButtonPressed(Input.MouseButton.RightButton)) onMouseRightPress();
            //Console.WriteLine($"Left={leftClickDragInProgress}, middle={middleClickDragInProgress}, right={rightClickDragInProgress}");


            // do last
            previouslyContainedMouse = nowContainsMouse;
        }

        void onMouseEnter() {
            OnMouseEnter?.Invoke();
        }

        void onMouseStay() {
            // Mouse pressed in
            if (Input.MouseButtonPressedIn(Input.MouseButton.LeftButton)) onMouseLeftPressIn();
            if (Input.MouseButtonPressedIn(Input.MouseButton.MiddleButton)) onMouseMiddlePressIn();
            if (Input.MouseButtonPressedIn(Input.MouseButton.RightButton)) onMouseRightPressIn();


            // Mouse clicked (pressed in then released)
            if (Input.MouseButtonClicked(Input.MouseButton.LeftButton)) onMouseLeftClick();
            if (Input.MouseButtonClicked(Input.MouseButton.MiddleButton)) onMouseMiddleClick();
            if (Input.MouseButtonClicked(Input.MouseButton.RightButton)) onMouseRightClick();

            // Mouse released
            if (Input.MouseButtonReleased(Input.MouseButton.LeftButton)) onMouseLeftRelease();
            if (Input.MouseButtonReleased(Input.MouseButton.MiddleButton)) onMouseMiddleRelease();
            if (Input.MouseButtonReleased(Input.MouseButton.RightButton)) onMouseRightRelease();

            // Mouse pressed
            if (Input.MouseButtonPressed(Input.MouseButton.LeftButton)) onMouseLeftPress();
            if (Input.MouseButtonPressed(Input.MouseButton.MiddleButton)) onMouseMiddlePress();
            if (Input.MouseButtonPressed(Input.MouseButton.RightButton)) onMouseRightPress();

            // Vertical scroll wheel
            int vertScroll = Input.MouseScrollWheelDelta(Input.ScrollWheelAxis.Y);
            if (vertScroll != 0) onMouseWheelVerticalScroll(vertScroll);

            // Horizontal scroll wheel
            int horizScroll = Input.MouseScrollWheelDelta(Input.ScrollWheelAxis.X);
            if (horizScroll != 0) onMouseWheelHorizontalScroll(horizScroll);

            OnMouseStay?.Invoke();
        }

        private void onMouseWheelHorizontalScroll(int scrollAmount)
        {
            OnMouseHorizontalScroll?.Invoke(scrollAmount);
        }

        private void onMouseWheelVerticalScroll(int scrollAmount)
        {
            OnMouseVerticalScroll?.Invoke(scrollAmount);
        }

        void onMouseLeave() {
            OnMouseLeave?.Invoke();
        }


        void onMouseLeftPressIn()
        {
            leftClickDragInProgress = true;
            OnMouseLeftPressIn?.Invoke();
        }

        void onMouseMiddlePressIn()
        {
            middleClickDragInProgress = true;
            OnMouseMiddlePressIn?.Invoke();
        }

        void onMouseRightPressIn()
        {
            rightClickDragInProgress = true;
            OnMouseRightPressIn?.Invoke();
        }


        void onMouseLeftClick()
        {
            OnMouseLeftClick?.Invoke();
        }

        void onMouseMiddleClick()
        {
            OnMouseMiddleClick?.Invoke();
        }

        void onMouseRightClick()
        {
            OnMouseRightClick?.Invoke();
        }


        void onMouseLeftRelease()
        {
            leftClickDragInProgress = false;
            OnMouseLeftRelease?.Invoke();
        }

        void onMouseMiddleRelease()
        {
            middleClickDragInProgress = false;
            OnMouseMiddleRelease?.Invoke();
        }

        void onMouseRightRelease()
        {
            rightClickDragInProgress = false;
            OnMouseRightRelease?.Invoke();
        }


        void onMouseLeftPress()
        {
            if (Input.MouseMovementDelta() != Vector2.Zero)
                onMouseDrag(Input.MouseButton.LeftButton, Input.MouseMovementDelta());

            OnMouseLeftPress?.Invoke();
        }

        void onMouseMiddlePress()
        {
            if (Input.MouseMovementDelta() != Vector2.Zero)
                onMouseDrag(Input.MouseButton.MiddleButton, Input.MouseMovementDelta());

            OnMouseMiddlePress?.Invoke();
        }

        void onMouseRightPress()
        {
            if (Input.MouseMovementDelta() != Vector2.Zero)
                onMouseDrag(Input.MouseButton.RightButton, Input.MouseMovementDelta());

            OnMouseRightPress?.Invoke();
        }

        void onMouseDrag(Input.MouseButton button, Vector2 movement)
        {
            OnMouseDrag?.Invoke(button, movement);
        }
    }
}
