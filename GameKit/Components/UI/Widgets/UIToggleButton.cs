﻿using System;
using Microsoft.Xna.Framework;
using GameKit.Tweening;

namespace GameKit.Components.UI.Widgets
{
    public class UIToggleButton : UIButton
    {
        public Color normalTintColor_off = new Color(0.5f, 0.5f, 0.5f);
        public Color highlightedTintColor_off = new Color(0.55f, 0.55f, 0.55f);
        public Color pressedTintColor_off = new Color(0.4f, 0.4f, 0.4f);

        public Color normalTintColor_on = new Color(0.8f, 0.8f, 0.8f);
        public Color highlightedTintColor_on = new Color(0.85f, 0.85f, 0.85f);
        public Color pressedTintColor_on = new Color(0.75f, 0.75f, 0.75f);

        protected bool on = false;

        public bool State
        {
            get { return on; }
            set { on = value; UpdateState(); }
        }

        public UIToggleButton()
        {
            OnButtonPressed += ToggleButton;
        }

        public void ToggleButton()
        {
            on = !on;
            UpdateState();
        }

        public void SetState(bool state)
        {
            on = state;
            UpdateState();
            UpdateButtonStateIdle();
        }

        public override void Press()
        {
            on = !on;
            UpdateState();
            base.Press();
        }

        public virtual void UpdateState() {
            if (on)
            {
                normalTintColor = normalTintColor_on;
                highlightedTintColor = highlightedTintColor_on;
                
            } else
            {
                normalTintColor = normalTintColor_off;
                highlightedTintColor = highlightedTintColor_off;
            }
        }
    }
}
