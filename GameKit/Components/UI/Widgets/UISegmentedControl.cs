﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

using GameKit.Components.Drawing;
using GameKit.Tweening;

namespace GameKit.Components.UI.Widgets
{
    public class UISegmentedControl : UIComponent
    {
        private ConstrainedValue current = new ConstrainedValue(0);

        protected List<UIButton> buttons = new List<UIButton>();

        public int CurrentIndex
        {
            get
            {
                return (int)current.Value;
            }

            set
            {
                current.SetValue(value);
                UpdateCurrentButton();
            }
        }

        public int TotalItems
        {
            get
            {
                return (int)current.Maximum;
            }

            set
            {
                current.SetMax(value);
            }
        }

        public UIButton CurrentButton
        {
            get
            {
                return buttons[CurrentIndex];
            }
        }

        public virtual void UpdateCurrentButton() { }
    }
}
