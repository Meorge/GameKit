﻿using System;
using Microsoft.Xna.Framework;

using GameKit.Components.Drawing;
using GameKit.Tweening;

namespace GameKit.Components.UI.Widgets
{
    public class UIButton : UIComponent
    {
        public Color normalTintColor = new Color(0.5f, 0.5f, 0.5f);
        public Color highlightedTintColor = new Color(0.55f, 0.55f, 0.55f);
        public Color pressedTintColor = new Color(0.4f, 0.4f, 0.4f);

        ButtonState buttonState = ButtonState.Idle;

        // TODO: make an interface for renderers
        public SpriteRenderer spriteRenderer = null;
        public GameObject scaleTarget = null;

        public float normalScale = 1f;
        public float hoverScale = 1f;
        public float pressScale = 0.93f;

        public float animationDuration = 0.05f;
        public float colorAnimationDuration = 0.3f;

        public delegate void ButtonPressEvent();

        public event ButtonPressEvent OnButtonPressed;
        public event ButtonPressEvent OnButtonHeldDown;

        public override void Create()
        {
            OnMouseEnter += onMouseEnter;
            OnMouseLeave += onMouseLeave;

            OnMouseLeftPressIn += onMouseLeftPressIn;
            OnMouseLeftRelease += onMouseLeftRelease;

            if (spriteRenderer == null)
                spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

            if (scaleTarget == null)
                scaleTarget = gameObject;

            setState(ButtonState.Idle);
        }

        void onMouseEnter()
        {
            setState(ButtonState.Hover);
        }

        void onMouseLeave()
        {
            setState(ButtonState.Idle);
        }

        void onMouseLeftPressIn()
        {
            setState(ButtonState.Press);
        }

        void onMouseLeftRelease()
        {
            setState(ButtonState.Hover);
        }

        void setState(ButtonState state)
        {
            ButtonState prevState = buttonState;
            buttonState = state;


            switch (state)
            {
                case ButtonState.Hover:
                    if (prevState == ButtonState.Press)
                    {
                        Console.WriteLine("Invoke button pressed");
                        OnButtonPressed?.Invoke();
                    }
                    tweenToColor(highlightedTintColor);
                    tweenScaleHover();

                    break;
                case ButtonState.Press:
                    OnButtonHeldDown?.Invoke();
                    tweenToColor(pressedTintColor);
                    tweenScalePressIn();
                    break;
                default:
                    tweenToColor(normalTintColor);
                    tweenScaleLeave();
                    break;
            }
            
        }

        public virtual void Press() {
            tweenToColor(normalTintColor);
            OnButtonPressed?.Invoke();
        }

        public void UpdateButtonStateIdle()
        {
            setState(ButtonState.Idle);
        }

        void tweenToColor(Color target)
        {
            _ = new TweenSequence()
                .Then(Tween.To((float)target.R / 255f, spriteRenderer.GetTweenColorR(), colorAnimationDuration, Ease.EaseInOutQuad))
                .And(Tween.To((float)target.G / 255f, spriteRenderer.GetTweenColorG(), colorAnimationDuration, Ease.EaseInOutQuad))
                .And(Tween.To((float)target.B / 255f, spriteRenderer.GetTweenColorB(), colorAnimationDuration, Ease.EaseInOutQuad))
                .And(Tween.To((float)target.A / 255f, spriteRenderer.GetTweenOpacity(), colorAnimationDuration, Ease.EaseInOutQuad))
                .Start();
        }

        void tweenScaleHover()
        {
            if (scaleTarget == null) return;
            _ = new TweenSequence()
                .Then(Tween.To(hoverScale, scaleTarget.GetTweenXScale(), animationDuration, Ease.EaseOutQuart))
                .And(Tween.To(hoverScale, scaleTarget.GetTweenYScale(), animationDuration, Ease.EaseOutQuart))
                .Start();
        }

        void tweenScalePressIn()
        {
            if (scaleTarget == null) return;
            _ = new TweenSequence()
                .Then(Tween.To(pressScale, scaleTarget.GetTweenXScale(), animationDuration, Ease.EaseOutQuart))
                .And(Tween.To(pressScale, scaleTarget.GetTweenYScale(), animationDuration, Ease.EaseOutQuart))
                .Start();
        }

        void tweenScaleLeave()
        {
            if (scaleTarget == null) return;
            _ = new TweenSequence()
                .Then(Tween.To(normalScale, scaleTarget.GetTweenXScale(), animationDuration, Ease.EaseOutQuart))
                .And(Tween.To(normalScale, scaleTarget.GetTweenYScale(), animationDuration, Ease.EaseOutQuart))
                .Start();
        }

        public enum ButtonState
        {
            Idle,
            Hover,
            Press
        }
    }
}
