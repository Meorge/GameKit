﻿using System;
namespace GameKit.Components.UI
{
    public static class UIAbstractAlignerFluentExtensions
    {
        public static T SetPadding<T>(this T aligner, int padding) where T : UIAbstractAligner
        {
            aligner.Padding = padding;
            return aligner;
        }
    }
}
