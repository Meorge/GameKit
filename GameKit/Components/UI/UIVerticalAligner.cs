﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Components.UI
{
    public class UIVerticalAligner : UIAbstractAligner
    {
        public override void Update(GameTime gameTime)
        {
            // Get the height of the children all together
            int heightSum = 0;
            foreach (GameObject child in gameObject.Children)
            {
                child.Y = (heightSum * 2) - (int)(gameObject.Pivot.Y * gameObject.Height);
                heightSum += child.Height / 2;
                heightSum += Padding;
            }
            // remove the last spacing
            heightSum -= Padding;

            // Find distance from children sum center to parent center
            int distance = (gameObject.Height / 2) - heightSum;

            //Console.WriteLine($"UIVerticalAligner -- gameObject.Height / 2 ({gameObject.Height / 2}) - heightSum ({heightSum}) = {distance}");

            // Add this distance to all of the childrens' Ys
            foreach (GameObject child in gameObject.Children)
            {
                child.Y += distance;
                //Console.WriteLine($"UIVerticalAligner -- Child {child.name} is at {child.Y}");
            }
            //Console.WriteLine("UIVerticalAligner -- Done");
        }
    }
}
