﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

using GameKit.Components.Drawing;

namespace GameKit
{
    public class GameObject : IDisposable
    {
        /// <summary>
        /// The name of this GameObject.
        /// </summary>
        public string name = "GameObject";

        // Components
        private List<Component> components = new List<Component>();

        // Transform

        public Rectangle rectangle;
        private int zPos;
        private float zRanged;


        /// <summary>
        /// The scale of this GameObject.
        /// </summary>
        public Vector2 scale;
        private float rotation;

        private Vector2 pivot = Vector2.Zero;

        /// <summary>
        /// The position space (either World or Camera) of this GameObject.
        /// </summary>
        public PositionSpace positionSpace = PositionSpace.WorldSpace;

        // parenting stuff
        private List<GameObject> children = new List<GameObject>();
        private GameObject parent = null;

        /// <summary>
        /// The position of this GameObject. Keep in mind that the positionSpace will impact where the GameObject is displayed.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return new Vector3(rectangle.X, rectangle.Y, zPos);
            }

            set
            {
                X = (int)value.X;
                Y = (int)value.Y;
                if (Z != (int)value.Z) Z = (int)value.Z;
            }
        }

        /// <summary>
        /// The X and Y coordinates of the GameObject's position. Keep in mind that the postitionSpace will impact where the GameObject is displayed.
        /// </summary>
        public Vector2 Position2D
        {
            get
            {
                return new Vector2(rectangle.X, rectangle.Y);
            }
            set
            {
                X = (int)value.X;
                Y = (int)value.Y;
            }
        }

        /// <summary>
        /// The X and Y coordinates of this GameObject, adjusted for its pivot.
        /// </summary>
        public Vector2 Position2D_PivotAdjusted
        {
            get
            {
                //Vector3 _scl;
                //Quaternion _rot;
                //Vector3 _trs;
                
                //TransformationMatrix.Decompose(out _scl, out _rot, out _trs);

                //TransformationMatrix.Tra

                Vector2 v = new Vector2(rectangle.X + (Pivot.X * rectangle.Width), rectangle.Y + (Pivot.Y * rectangle.Height));
                return v;
            }

        }

        /// <summary>
        /// The pivot of this GameObject, normalized across its dimensions. (0,0) represents the top left corner of its bounding rectangle, and (1,1) represents the bottom right corner.
        /// Make sure to set the GameObject's dimensions first!
        /// </summary>
        public Vector2 Pivot
        {
            get
            {
                return pivot;
            }

            set
            {
                pivot = Vector2.Clamp(value, Vector2.Zero, Vector2.One);
            }
        }

        /// <summary>
        /// The X and Y coordinates of this GameObject's absolute position (adjusted for parent hierarchy).
        /// </summary>
        public Vector2 AbsolutePosition
        {
            get
            {
                Vector3 v = TransformationMatrix.Translation;
                //Console.WriteLine($"{name} : Effective pos {Position2D_PivotAdjusted}, Absolute pos {v}");
                return new Vector2(v.X, v.Y);
            }
        }

        /// <summary>
        /// The X coordinate of this GameObject's position. Make sure you set the GameObject's pivot before this!
        /// </summary>
        public int X
        {
            get
            {
                return rectangle.X + (int)(Width * Pivot.X);
            }

            set
            {
                int outVal = value - (int)(Width * Pivot.X);

                //Console.WriteLine($"X was {rectangle.X + (int)(Width * Pivot.X)}, now it's {value} - (int)({Width} * {Pivot.X})");
                rectangle.X = (int)(outVal);
            }
        }

        /// <summary>
        /// The Y coordinate of this GameObject's position. Make sure you set the GameObject's pivot before this!
        /// </summary>
        public int Y
        {
            get
            {
                return rectangle.Y + (int)(Height * Pivot.Y);
            }

            set
            {
                //Console.WriteLine($"GAMEOBJECT - {name} Y = {value}");
                int outVal = value - (int)(Height * Pivot.Y);
                rectangle.Y = (int)(outVal);
            }
        }


        public int RawX
        {
            get
            {
                return rectangle.X;
            }

            set
            {
                rectangle.X = value;
            }
        }

        public int RawY
        {
            get
            {
                return rectangle.Y;
            }

            set
            {
                rectangle.Y = value;
            }
        }

        /// <summary>
        /// The Z value of this GameObject. Objects with a larger Z value will be rendered on top of those with a smaller Z value.
        /// </summary>
        public int Z
        {
            get
            {
                return zPos;
            }

            set
            {
                //Console.WriteLine($"Z position of {name} modified from {zPos} to {value}");
                zPos = value;
                
            }
        }


        public int CumulativeZ
        {
            get
            {
                int zOut = Z;
                if (Parent != null) zOut += Parent.CumulativeZ;
                return zOut;
            }
        }

        /// <summary>
        /// The Z value of this GameObject, adjusted into a 0-1 range. Only really useful for the engine.
        /// </summary>
        public float ZRange
        {
            get
            {
                return zRanged;
            }
        }

        public Rectangle AbsoluteRectangle
        {
            get { return new Rectangle(AbsolutePosition.X.RoundToInt() - (int)(Pivot.X * Width * AbsoluteScale.X), AbsolutePosition.Y.RoundToInt() - (int)(Pivot.Y * Height * AbsoluteScale.Y), (int)(Width * AbsoluteScale.X), (int)(Height * AbsoluteScale.Y)); }
        }

        /// <summary>
        /// This rotation of this GameObject in radians.
        /// </summary>
        public float Rotation
        {
            get
            {
                return rotation;
            }

            set
            {
                rotation = value;
            }
        }

        /// <summary>
        /// The absolute rotation of this GameObject in radians (including parent rotations).
        /// </summary>
        public float AbsoluteRotation
        {
            get
            {
                Vector3 _s, _t;
                Quaternion rotation;
                TransformationMatrix.Decompose(out _s, out rotation, out _t);

                return rotation.EulerZ();
            }
        }

        /// <summary>
        /// The absolute scale of this GameObject (including parent scale).
        /// </summary>
        public Vector3 AbsoluteScale
        {
            get
            {
                Vector3 _scale, _t;
                Quaternion _r;
                TransformationMatrix.Decompose(out _scale, out _r, out _t);
                return _scale;
            }
        }

        /// <summary>
        /// The height of this GameObject's rectangle. Make sure you set this before the pivot!
        /// </summary>
        public int Height
        {
            get
            {
                return rectangle.Height;
            }

            set
            {
                rectangle.Height = value;
            }
        }

        /// <summary>
        /// The width of this GameObject's rectangle. Make sure you set this before the pivot!
        /// </summary>
        public int Width
        {
            get
            {
                return rectangle.Width;
            }

            set
            {
                rectangle.Width = value;
            }
        }

        /// <summary>
        /// The dimensions (both height and width) of this GameObject. Make sure you set this before the pivot!
        /// </summary>
        public Vector2 Dimensions
        {
            get
            {
                return new Vector2(rectangle.Width, rectangle.Height);
            }

            set
            {
                Width = (int)value.X;
                Height = (int)value.Y;
            }
        }

        /// <summary>
        /// The dimensions (both height and width) of this GameObject, adjusted for its local scale.
        /// </summary>
        public Vector2 ScaledDimensions
        {
            get
            {
                return Dimensions * AbsoluteScale.ToVector2();
            }
        }

        /// <summary>
        /// The parent of this GameObject, if it exists. This GameObject will inherit the transformations made to its parent.
        /// </summary>
        public GameObject Parent
        {
            get
            {
                return parent;
            }

            set
            {
                if (parent != null) parent.children.Remove(this);
                if (value != null) value.children.Add(this);
                parent = value;
            }
        }

        /// <summary>
        /// A list of GameObjects that are parented to this GameObject.
        /// </summary>
        public List<GameObject> Children
        {
            get
            {
                return new List<GameObject>(children);
            }
        }

        /// <summary>
        /// The transformation matrix containing absolute transformation information for this GameObject.
        /// </summary>
        public Matrix TransformationMatrix
        {
            get
            {
                //Console.WriteLine($"Transformation matrix for {name}");
                Matrix myTransformations =
                    //Engine.engine.ScreenScaleMatrix *
                    Matrix.CreateScale(scale.X, scale.Y, 1f) *
                    Matrix.CreateRotationZ(Rotation) *
                    Matrix.CreateTranslation(Position2D_PivotAdjusted.X, Position2D_PivotAdjusted.Y, 0f);

                if (parent == null)
                {
                    //Console.WriteLine("I'm top level so just return my matrix");
                    return myTransformations;
                }
                else
                {
                    //Console.WriteLine($"My parent is {parent.name} so multiplying by their matrix");
                    return myTransformations * parent.TransformationMatrix;
                }
            }
        }




        // Constructors

        /// <summary>
        /// Creates a new GameObject with a position of (0,0,0) and a scale of (1,1).
        /// </summary>
        public GameObject()
        {
            Position = Vector3.Zero;
            scale = Vector2.One;
        }

        /// <summary>
        /// Creates a new GameObject with a scale of (1,1).
        /// </summary>
        /// <param name="pos">The position of the GameObject.</param>
        public GameObject(Vector3 pos)
        {
            Position = pos;
            scale = Vector2.One;
        }

        /// <summary>
        /// Creates a new GameObject.
        /// </summary>
        /// <param name="pos">The position of the GameObject.</param>
        /// <param name="scl">The scale of the GameObject.</param>
        public GameObject(Vector3 pos, Vector2 scl)
        {
            Position = pos;
            scale = scl;
        }

        /// <summary>
        /// Creates a new GameObject.
        /// </summary>
        /// <param name="pos">The position of the GameObject.</param>
        /// <param name="scl">The scale of the GameObject.</param>
        /// <param name="dim">The dimensions of the GameObject.</param>
        public GameObject(Vector3 pos, Vector2 scl, Vector2 dim)
        {
            Position = pos;
            Dimensions = dim;
            scale = scl;
        }

        // Name stuff

        /// <summary>
        /// Sets the name of the GameObject.
        /// </summary>
        /// <param name="n">New name of GameObject</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetName(string n)
        {
            name = n;
            return this;
        }



        // Transform stuff


        /// <summary>
        /// Sets the position of the GameObject.
        /// </summary>
        /// <param name="x">New X coordinate</param>
        /// <param name="y">New Y coordinate</param>
        /// <param name="z">New Z coordinate (depth)</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPosition(int x, int y, int z)
        {
            Position = new Vector3(x, y, z);
            return this;
        }

        /// <summary>
        /// Sets the position of the GameObject.
        /// </summary>
        /// <param name="x">New X coordinate</param>
        /// <param name="y">New Y coordinate</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPosition(int x, int y)
        {
            SetPosition(x, y, Z);
            return this;
        }

        /// <summary>
        /// Sets the position of the GameObject.
        /// </summary>
        /// <param name="v">New X and Y coordinates</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPosition(Vector2 v)
        {
            Position = new Vector3(v.X, v.Y, Z);
            return this;
        }

        /// <summary>
        /// Sets the position of the GameObject.
        /// </summary>
        /// <param name="v">New X, Y, and Z coordinates</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPosition(Vector3 v)
        {
            Position = v;
            return this;
        }

        /// <summary>
        /// Sets the rotation of the GameObject, in radians.
        /// </summary>
        /// <param name="r">New rotation value (in radians)</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetRotation(float r)
        {
            Rotation = r;
            return this;
        }

        /// <summary>
        /// Sets the local scale of the GameObject.
        /// </summary>
        /// <param name="x">New X scale</param>
        /// <param name="y">New Y scale</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetScale(float x, float y)
        {
            scale = new Vector2(x, y);
            return this;
        }

        /// <summary>
        /// Sets the local pivot of the GameObject, normalized. (0,0) represents the top left corner of the GameObject's rectangle, and (1,1) represents the bottom right corner.
        /// </summary>
        /// <param name="x">New X pivot</param>
        /// <param name="y">New Y pivot</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPivot(float x, float y)
        {
            Pivot = new Vector2(x, y);
            return this;
        }

        /// <summary>
        /// Sets the local pivot of the GameObject, normalized. (0,0) represents the top left corner of the GameObject's rectangle, and (1,1) represents the bottom right corner.
        /// </summary>
        /// <param name="p">New X and Y pivots</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPivot(Vector2 p)
        {
            Pivot = p;
            return this;
        }

        /// <summary>
        /// Sets the Z value, or depth, of the GameObject. GameObjects with a higher Z value will be drawn on top of those with a lower Z value.
        /// </summary>
        /// <param name="z">New Z value</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetDepth(int z)
        {
            Z = z;
            return this;
        }

        /// <summary>
        /// Sets the dimensions of the GameObject.
        /// </summary>
        /// <param name="w">New width</param>
        /// <param name="h">New height</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetDimensions(int w, int h)
        {
            Width = w;
            Height = h;
            return this;
        }

        /// <summary>
        /// Sets the position space of the GameObject. In world space, GameObjects are drawn in the world, and moving the camera around will change where they appear on the screen.
        /// In camera space, GameObject's positions will always be in the same place on the viewport.
        /// </summary>
        /// <param name="s">New position space</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetPositionSpace(PositionSpace s)
        {
            positionSpace = s;
            return this;
        }

        /// <summary>
        /// Sets the parent of the GameObject. If a GameObject has a parent, it will inherit its transformations.
        /// </summary>
        /// <param name="p">The GameObject to make this a child of</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject SetParent(GameObject p)
        {
            Parent = p;
            return this;
        }

        /// <summary>
        /// Looks for a child of this GameObject with a given name.
        /// Note that this function is not recursive - it will only search one "level" down.
        /// </summary>
        /// <param name="childName">The name of the child you want to find</param>
        /// <returns>Reference to the child GameObject with name childName, if it exists; null otherwise</returns>
        public GameObject FindChild(string childName)
        {
            return Children.Find((a) => a.name == childName);
        }


        /// <summary>
        /// Rotates the GameObject to "look at" a given point.
        /// </summary>
        /// <param name="pt">The point for the GameObject to point towards</param>
        /// <returns>Reference to this GameObject</returns>
        public GameObject LookAt(Vector2 pt)
        {
            float rot = MathF.Atan2(pt.Y - Y, pt.X - X);
            rot += MathF.PI / 2f;
            rot = rot >= 0f ? rot : (2 * MathF.PI) + rot;
            Rotation = rot;
            return this;
        }

        /// <summary>
        /// Determines the vector pointing from the GameObject's position to a given point.
        /// </summary>
        /// <param name="pt">The point to find the "look vector" from the GameObject to.</param>
        /// <returns>A vector pointing from the GameObject's position to the given point.</returns>
        public Vector2 GetLookVector(Vector2 pt)
        {
            float rot = MathF.Atan2(pt.Y - Y, pt.X - X);
            rot += MathF.PI / 2f;
            rot = rot >= 0f ? rot : (2 * MathF.PI) + rot;

            return new Vector2(MathF.Cos(rot), MathF.Sin(rot));
        }

        /// <summary>
        /// Converts a point in the GameObject's local space to world space (only rotation though??)
        /// </summary>
        /// <param name="v">The point in local space to convert to world space.</param>
        /// <returns></returns>
        public Vector2 LocalSpaceToWorldSpace(Vector2 v)
        {
            return Vector2.Transform(v, Matrix.CreateRotationZ(Rotation));
        }

        // Component stuff

        /// <summary>
        /// Adds a new component of the given type to the GameObject.
        /// Note that if there is already a component of this type on the GameObject, a new one won't be added and this function will return null.
        /// </summary>
        /// <typeparam name="T">The type of component to add to this GameObject</typeparam>
        /// <returns>The instance of the component, if it was added; null if it was not added (because a component of its type is already attached).</returns>
        public T AddComponent<T>() where T: Component
        {
            // Check to make sure that component isn't already added
            foreach (Component comp in components)
            {
                if (comp is T)
                {
                    return null;
                }
            }

            // Create and add the component, then return it
            T newComp = Activator.CreateInstance<T>();
            components.Add(newComp);

            newComp.gameObject = this;
            return newComp;
        }

        /// <summary>
        /// Attempts to get and return the instance of the component of the given type attached to the GameObject.
        /// </summary>
        /// <typeparam name="T">The type of component to look for</typeparam>
        /// <returns>The instance of the found component, if it was found; null otherwise</returns>
        public T GetComponent<T>() where T: Component
        {
            foreach (Component comp in components)
            {
                if (comp is T)
                {
                    return (T)comp;
                }
            }

            return null;
        }







        // Internal stuff

        public void UpdateZRange()
        {
            zRanged = GKMath.InverseLerp(Engine.engine.gameObjects[0].CumulativeZ, Engine.engine.gameObjects[Engine.engine.gameObjects.Count - 1].CumulativeZ, CumulativeZ);
        }

        public void Update(GameTime gameTime)
        {
            List<Component> cC = new List<Component>(components);
            foreach (Component comp in cC)
                comp._InternalUpdate(gameTime);
        }

        public void Draw(GameTime gameTime, Camera camera)
        {
            List<Component> cC = new List<Component>(components);
            foreach (Component comp in cC)
                comp._InternalDraw(gameTime, camera);
        }

        public void Dispose()
        {
            parent.children.Remove(this);
            foreach (Component c in components) c.Dispose();
            components.Clear();
            //throw new NotImplementedException();
        }
    }
}
