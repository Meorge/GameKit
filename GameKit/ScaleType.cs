﻿using System;
namespace GameKit
{
    public enum PositionSpace
    {
        WorldSpace = 0,
        CameraSpace = 1
    }
}
