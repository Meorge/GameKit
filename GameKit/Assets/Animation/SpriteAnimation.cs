﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GameKit.Assets.Animation
{
    public class SpriteAnimation : Asset
    {
        public string prefix;

        public int initialStart = 0;
        public int loopStart = 0;
        public int loopEnd = 0;
        public IList<SpriteAnimationFrame> frames;

        public static SpriteAnimation FromJSON(string filePath)
        {
            string jsonData = System.IO.File.ReadAllText($"./Content/{filePath}");

            return JsonConvert.DeserializeObject<SpriteAnimation>(jsonData);
        }

        public List<string> AllFrames()
        {
            List<string> frameNames = new List<string>();
            foreach (SpriteAnimationFrame f in frames)
            {
                string fName = prefix + f.name;
                if (!frameNames.Contains(fName)) frameNames.Add(fName);
            }
            return frameNames;
        }
    }

    public class SpriteAnimationFrame
    {
        public string name;
        public float duration;
    }
}
