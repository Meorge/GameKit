﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit.Assets
{

    public class AbstractSprite : Asset
    {
        public string sourceTexture;
    }
    public class Sprite : AbstractSprite
    {
        public Rectangle sourceRectangle;

        public Sprite(string t, Rectangle r)
        {
            sourceTexture = t;
            sourceRectangle = r;
        }
    }

    public class NineSliceSprite : AbstractSprite
    {
        public int topPadding, leftPadding, rightPadding, bottomPadding;
    }
}
