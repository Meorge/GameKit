﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace GameKit.Assets
{
    public class Font : Asset
    {
        public SpriteFont spriteFont;
    }
}
