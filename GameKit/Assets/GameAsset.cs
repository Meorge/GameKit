﻿using System;
using System.Collections.Generic;

namespace GameKit.Assets
{
    public class GameAsset
    {
        public string type;
        public string filename;
        public string id = null;
    }

    public class GameAssetPack
    {
        public IList<GameAsset> assets;
    }
}
