﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace GameKit
{
    public class Timer : IDisposable
    {
        public static List<Timer> timers = new List<Timer>();

        private ConstrainedValue _value;
        private bool _repeats = false;
        private bool _fired = false;

        public delegate void TimerFinished();
        public event TimerFinished onTimerFinished;

        private bool _disposed = false;

        public bool Depleted
        {
            get
            {
                return _value.Value == _value.Minimum;
            }
        }

        public static Timer Empty()
        {
            return new Timer(0f);
        }

        public Timer(float defaultTime, TimerFinished onTimerFinish = null, bool repeats = false)
        {
            _value = new ConstrainedValue(defaultTime);
            _repeats = repeats;

            

            if (onTimerFinish != null) onTimerFinished += onTimerFinish;

            timers.Add(this);
        }

        public void Update(GameTime gameTime)
        {
            if (_disposed)
            {
                Console.WriteLine("Trying to run Update() on a disposed timer, thats a no no uwu");
            }

            float current = _value.DecrementBy((float)gameTime.ElapsedGameTime.TotalSeconds);
            if (current == 0 && !_fired)
            {
                _fired = true;
                if (onTimerFinished != null) onTimerFinished.Invoke();

                if (_repeats)
                {
                    _value.ResetToMax();
                    _fired = false;
                } else
                {
                    Dispose();
                }
            }
        }

        public void Dispose()
        {
            timers.Remove(this);
            _disposed = true;
        }


    }
}
