﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit
{
    public class BaseGame : Game
    {
        public BaseGame()
        {
            Content.RootDirectory = "Content";
            _ = new Engine(this);
        }

        protected override void Initialize()
        {
            base.Initialize();
            Start();
        }

        protected override void LoadContent()
        {
            Engine.engine.LoadContent();
            Load();
        }

        protected override void Update(GameTime gameTime)
        {
            Engine.engine.Update(gameTime);
            base.Update(gameTime);
            Tick(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            Engine.engine.Draw(gameTime);
            base.Draw(gameTime);
        }

        /// <summary>
        /// Called when the game first starts. Use this function to set up your scene.
        /// </summary>
        virtual public void Start() { }

        /// <summary>
        /// Called after Start(). Use this function to load assets.
        /// </summary>
        virtual public void Load() { }


        /// <summary>
        /// Called every frame. Use this function to update your game state.
        /// </summary>
        /// <param name="gameTime">Contains information about the game time (including the length of time since the last call to Tick() via GameTime.ElapsedGameTime).</param>
        virtual public void Tick(GameTime gameTime) { }

    }
}
