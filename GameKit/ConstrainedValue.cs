﻿using System;
using Microsoft.Xna.Framework;

namespace GameKit
{
    public class ConstrainedValue
    {
        private float _internalValue;

        private float _min;
        private float _max;

        public ConstrainedValue(float defaultValue, float minimum = 0f, bool defaultToMinimum = false)
        {
            _min = minimum;
            _max = defaultValue;
            _internalValue = defaultToMinimum ? minimum : defaultValue;
        }

        public float ResetToMin()
        {
            _internalValue = _min;
            return _internalValue;
        }

        public float ResetToMax()
        {
            _internalValue = _max;
            return _internalValue;
        }

        public void SetMax(float newMax)
        {
            _max = newMax;
            SetValue(Value);
        }

        public void SetMin(float newMin)
        {
            _min = newMin;
            SetValue(Value);
        }

        public float SetValue(float value)
        {
            _internalValue = MathHelper.Clamp(value, _min, _max);
            return _internalValue;
        }

        public float DecrementBy(float amount)
        {
            _internalValue = MathHelper.Clamp(_internalValue - amount, _min, _max);
            return _internalValue;
        }

        public float IncrementBy(float amount)
        {
            _internalValue = MathHelper.Clamp(_internalValue + amount, _min, _max);
            return _internalValue;
        }

        public float Value
        {
            get
            {
                return _internalValue;
            }
        }

        public float Minimum
        {
            get
            {
                return _min;
            }
        }

        public float Maximum
        {
            get
            {
                return _max;
            }
        }

        public bool IsFull
        {
            get
            {
                return _internalValue == _max;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return _internalValue == _min;
            }
        }

        public float Percentage
        {
            get
            {
                return _internalValue / _max;
            }
        }
    }
}
